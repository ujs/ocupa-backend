from django.views.generic.base import TemplateView
from rest_auth.registration.views import SocialAccountListView, \
    SocialAccountDisconnectView
from rest_framework.routers import SimpleRouter
from django.conf.urls import url

from . import views

router = SimpleRouter()
router.register(r'political-front', views.PoliticalFrontViewSet),
router.register(r'activism-school-workplace', views.ActivismSchoolWorkplaceViewSet),
router.register(r'ujs-manager-role', views.UjsManagerRoleViewSet),
router.register(r'stats', views.StatsViewSet, base_name='user_stats'),
router.register(r'', views.UserViewSet),

urlpatterns = [
    url(
        regex=r'^key/$',
        view=views.get_api_key,
        name='api-key'
    ),
    url(
        regex=r'^details/(?P<username>.*)/$',
        view=views.SimpleUserViewSet,
        name='user-detail'
    ),
    url(r'^close/$', TemplateView.as_view(template_name='users/close.html')),
    url(r'^affiliation/$', views.UserAffiliationView.as_view()),

    url(
        r'^socialaccounts/$',
        SocialAccountListView.as_view(),
        name='social_account_list'
    ),
    url(
        r'^socialaccounts/(?P<pk>\d+)/disconnect/$',
        SocialAccountDisconnectView.as_view(),
        name='social_account_disconnect'
    )
]

urlpatterns.extend(router.urls)
