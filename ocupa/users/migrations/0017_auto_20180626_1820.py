# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-26 21:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0016_auto_20180530_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='sexual_orientation_other',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Other sexual orientation'),
        ),
        migrations.AlterField(
            model_name='user',
            name='gender',
            field=models.CharField(blank=True, choices=[('CIS_FEMALE', 'Cis Female'), ('CIS_MALE', 'Cis Male'), ('TRANSSEXUAL_WOMAN', 'Transsexual woman'), ('TRANSSEXUAL_MAN', 'Transsexual man'), ('NON-BINARY', 'Non binary'), ('TRANSGENDER', 'Transgender'), ('OTHER', 'Other')], max_length=255, null=True, verbose_name='Gender'),
        ),
        migrations.AlterField(
            model_name='user',
            name='sexual_orientation',
            field=models.CharField(blank=True, choices=[('LÉSBICA', 'Lesbian'), ('GAY', 'Gay'), ('BISSEXUAL', 'Bissexual'), ('HETEROSSEXUAL', 'Heterossexual'), ('ASSEXUAL', 'Assexual'), ('FLUIDO', 'Fluid'), ('OTHER', 'Other')], max_length=255, null=True, verbose_name='Sexual orientation'),
        ),
    ]
