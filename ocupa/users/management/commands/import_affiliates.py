# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.utils import IntegrityError
from ocupa.users.models import PoliticalFront, ActivismSchoolWorkplace, Event, UjsManagerRole
from cities_light.models import Country, Region, City

import csv
import re

User = get_user_model()


state_slugs = {
    'AC': 'Acre',
    'AL': 'Alagoas',
    'AP': 'Amapa',
    'AM': 'Amazonas',
    'BA': 'Bahia',
    'CE': 'Ceara',
    'ES': 'Espirito-Santo',
    'DF': 'Federal-District',
    'GO': 'Goias',
    'MA': 'Maranhao',
    'MT': 'Mato-Grosso',
    'MS': 'Mato-Grosso-do-Sul',
    'MG': 'Minas-Gerais',
    'PA': 'Para',
    'PB': 'Paraiba',
    'PR': 'Parana',
    'PE': 'Pernambuco',
    'PI': 'Piaui',
    'RJ': 'Rio-de-Janeiro',
    'RN': 'Rio-Grande-do-Norte',
    'RS': 'Rio-Grande-do-Sul',
    'RO': 'Rondonia',
    'RR': 'Roraima',
    'SC': 'Santa-Catarina',
    'SP': 'Sao-Paulo',
    'SE': 'Sergipe',
    'TO': 'Tocantins',
}


def clean(value):
    value = str(value)
    value = value.strip()
    if not value:
        return None
    return value


def get_gender(value):
    value = clean(value)
    if value:
        value = 'MALE' if value == 'M' else 'FEMALE'
    return value


def get_country(value):
    value = clean(value)
    if value:
        if value.lower() == 'brasil':
            value = 'Brazil'

        value = Country.objects.get(name__icontains=value)
    return value


def get_city_state(city, state):
    city = clean(city)
    state = clean(state)
    if state:
        state = state_slugs.get(state.upper())
        state = Region.objects.get(slug=state)
        if city:
            try:
                city = City.objects.get(name__icontains=city, region=state)
            except (City.DoesNotExist, City.MultipleObjectsReturned):
                print(city + " " + str(state))
                city = None
        else:
            city = None
    else:
        state = None
    return (city, state)


def get_google(google, hangout):
    google = clean(google)
    hangout = clean(hangout)
    return google if google else hangout


def get_political_front(value):
    value = clean(value)
    if value:
        value = PoliticalFront.objects.filter(name__in=value.split(', '))
    return value


def get_activism_school_workplace(value):
    value = clean(value)
    if value:
        value = ActivismSchoolWorkplace.objects.get(name__icontains=value)
    return value


def get_ujs_manager_role(value):
    value = clean(value)

    if value:
        if value.lower() == 'nenhuma' or value.lower() == 'não':
            value = 'Nenhum'

        value = UjsManagerRole.objects.get(name__icontains=value)

    return value


def get_boolean(value):
    value = clean(value)

    true_boolean = {
        'S': True,
        'Sim': True,
        's': True,
        'sim': True,
        'N': False,
        'Não': False,
        'Nao': False,
        'n': False,
        'não': False,
        'nao': False
    }

    return true_boolean.get(value) if value else value


class Command(BaseCommand):
    args = 'file'
    help = 'Import affiliates from the older UJS platform'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('file', nargs='+', type=str)

    @transaction.atomic
    def handle(self, *files, **options):
        if not len(options['file']) == 1:
            raise CommandError('No file to import')

        failed_imports = 0
        updated_users = 0
        duplicated_users = 0
        with open(options['file'][0], 'r') as csvfile:
            readf = csv.DictReader(csvfile, delimiter=';')
            for row in readf:

                email = clean(row.get('Email'))
                if not email or not re.match(r'[^@]+@[^@]+\.[^@]+', email) or email == 'sample@email.tst':
                    failed_imports = failed_imports + 1
                    print('Email not recognized: ' + email)
                    continue

                try:
                    with transaction.atomic():
                        user = User.objects.get(email=email)
                except User.MultipleObjectsReturned:
                    print('Multiple users with email: ' + str(email))
                    duplicated_users += 1
                    # Give up on importing this user's data
                    continue
                except User.DoesNotExist:
                    user = User.objects.create(email=email, username=email)

                # Data already stored in the platform must be preserved
                # Only data empty data fields must receive CSV data
                if not user.name and row.get('Nome'):
                    user.name = clean(row.get('Nome'))

                if not user.sexual_orientation and row.get('OrientacaoSexual'):
                    user.sexual_orientation = clean(row.get('OrientacaoSexual'))

                if not user.gender and row.get('Sexo'):
                    user.gender = get_gender(row.get('Sexo'))

                # user. = row.get('DataNascimento')
                if not user.phone_number and row.get('Telefone'):
                    user.phone_number = clean(row.get('Telefone'))

                if not user.phone_number_other and row.get('Celular'):
                    user.phone_number_other = clean(row.get('Celular'))

                if not user.address and row.get('Endereco'):
                    user.address = clean(row.get('Endereco'))

                if not user.address_number and row.get('Numero'):
                    user.address_number = clean(row.get('Numero'))

                if not user.address_extra and row.get('Complemento'):
                    user.address_extra = clean(row.get('Complemento'))

                if not user.neighborhood and row.get('Bairro'):
                    user.neighborhood = clean(row.get('Bairro'))

                if not user.state and row.get('Estado'):
                    user.city, user.state = get_city_state(row.get('Cidade'), row.get('Estado'))

                if not user.zipcode and row.get('CEP'):
                    user.zipcode = clean(row.get('CEP'))

                if not user.country and row.get('Brazi'):
                    user.country = Country.objects.get(name='Brazil')

                if not user.social_facebook and row.get('Facebook'):
                    user.social_facebook = clean(row.get('Facebook'))

                if not user.social_twitter and row.get('Twitter'):
                    user.social_twitter = clean(row.get('Twitter'))

                if not user.social_skype and row.get('Skype'):
                    user.social_skype = clean(row.get('Skype'))

                if not user.social_google and row.get('Hangout'):
                    user.social_google = get_google(row.get('Google'), row.get('Hangout'))

                if not user.site and row.get('Site'):
                    user.site = clean(row.get('Site'))

                # user.political_front.add(get_political_front(row.get('FrenteMovimento')))
                if not user.political_front_other and row.get('FrenteMovimentoOutro'):
                    user.political_front_other = clean(row.get('FrenteMovimentoOutro'))

                if not user.studies and row.get('Estuda'):
                    user.studies = get_boolean(row.get('Estuda'))

                if not user.school and row.get('Escola'):
                    user.school = clean(row.get('Escola'))

                # user.activism_school_workplace = get_activism_school_workplace(row.get('LocalEstudaTrab'))
                if not user.activism_school_workplace_other and row.get('LocalEstudaTrabOutro'):
                    user.activism_school_workplace_other = clean(row.get('LocalEstudaTrabOutro'))

                if not user.has_mandate and row.get('MilitaPossuimandato'):
                    user.has_mandate = get_boolean(row.get('MilitaPossuimandato'))

                if not user.mandate and row.get('MilitaPossuimandatoQual'):
                    user.mandate = clean(row.get('MilitaPossuimandatoQual'))

                # user = get_ujs_manager_role(row.get('ParticipaInstancia'))
                if not user.has_vote and row.get('Vota'):
                    user.has_vote = get_boolean(row.get('Vota'))

                if not user.vote_city and row.get('VotaMunicipio'):
                    user.vote_city = clean(row.get('VotaMunicipio'))

                # user = clean(row.get('DataCadastro'))
                # user = clean(row.get('DataAlteração'))
                # user = clean(row.get('Evento'))

                user.ujs_affiliate = True
                user.from_import = True

                user.save()
                updated_users += 1

        print('Failed imports: ' + str(failed_imports))
        print('Updated users: ' + str(updated_users))
        print('Duplicated users: ' + str(duplicated_users))
