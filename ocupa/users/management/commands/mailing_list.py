from allauth.account.models import EmailAddress
from django.contrib.auth import get_user_model
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.files.base import ContentFile
from django.core.management import BaseCommand
from django.db.models import Prefetch
from django.http import HttpResponse

from ocupa.users.models import PoliticalFront

User = get_user_model()
class Command(BaseCommand):
    def handle(self, *files, **options):
        users = User.objects \
            .select_related('city') \
            .select_related('state') \
            .prefetch_related(
            Prefetch('political_front',
                     queryset=PoliticalFront.objects.all(),
                     to_attr='political_front_list'))

        verified = list(EmailAddress.objects\
            .filter(verified=True)\
            .values_list('user_id', flat=True))

        rendered = 'email, name, phone_number, django_id, ' \
                   'city, state, from_import, ujs_affiliate, ' \
                   'political_front, verified, last_login, date_joined\r\n'

        exclude_contains = [',', '%', 'ã', '?', ' ', 'á', 'é', 'í', 'ô', '...',
                            '(', 'ç', '.@', 'º', '%', '#', '&', ']', ':', '/',
                            '!', '..', ';', '´', '\\', '\'', 'ó', 'ê', '@_',
                            '@.', '>', 'â', '³', '@-', '"']

        exclude_start = ['abuse@', 'admin@', 'billing@', 'compliance@', 'devnull@',
                         'dns@', 'ftp@', 'hostmaster@', 'inoc@', 'ispfeedback@',
                         'ispsupport@', 'list-request@', 'list@', 'maildaemon@',
                         'noc@', 'no-reply@', 'noreply@', 'null@', 'phish@',
                         'phishing@', 'postmaster@', 'privacy@', 'registrar@',
                         'root@', 'security@', 'spam@', 'support@', 'sysadmin@',
                         'tech@', 'undisclosed-recipients@', 'unsubscribe@',
                         'abc@', '123@', '1234@',  'xxx@', 'none@', 'abcd@',
                         'usenet@', 'uucp@' 'webmaster@', 'www@', '.', ]

        single_number = ['%s@' % (chr(x),) for x in range(48,58)]
        single_letter = ['%s@' % (chr(x),) for x in range(97,123)]

        exclude_start.extend(single_number)
        exclude_start.extend(single_letter)

        exclude_domains = []
        domains = staticfiles_storage.open('data/invalid_mail_domains')
        lines = domains.readlines()
        exclude_domains = [l.strip(b'\n').decode('utf-8') for l in lines]

        exclude_end = ['1', '2', '.', '.c0m']

        for e in exclude_domains:
            users = users.exclude(email__endswith=e)

        users = users.exclude(email='')

        for e in exclude_contains:
            users = users.exclude(email__icontains=e)

        for e in exclude_start:
            users = users.exclude(email__istartswith=e)

        for e in exclude_end:
            users = users.exclude(email__iendswith=e)

        items = []
        users = list(users.order_by('id'))
        for u in users:
            row = [
                u.email, u.name or '', u.phone_number or '', u.id,
                u.city and u.city.name or '', u.state and u.state.name or '',
                u.from_import or False, u.ujs_affiliate or False,
                ','.join([pf.name for pf in u.political_front_list]),
                u.id in verified, u.last_login or '', u.date_joined
            ]
            l_map = map(str, row)
            items.append('"' + '","'.join(l_map) + '"')

        rendered += '\r\n'.join(items)
        csv_file = open('base_usuarios_mailchimp.csv', 'wb')
        csv_file.write(bytes(rendered, 'utf-8'))
        csv_file.close()
