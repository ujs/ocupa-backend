from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from django.db import IntegrityError
from django.db.models import Count, F
from django.db.models.functions import Lower

User = get_user_model()

class Command(BaseCommand):
    help = 'Normalize users e-mail data: all lower case and unique'

    def handle(self, *files, **options):
        users = User.objects \
            .annotate(lower_email=Lower('email')) \
            .values('lower_email') \
            .annotate(email_count=Count('lower_email')) \
            .filter(email_count__gt=1)\
            .values_list('lower_email', flat=True)

        for u in users:
            duplicates = User.objects.filter(email__iexact=u).all()
            count = 0
            for d in duplicates:
                d.email = d.email.lower()
                try:
                    d.save()
                except IntegrityError:
                    d.email = '+sinalizado+{}@'\
                        .format(str(count))\
                        .join(d.email.split('@'))
                    d.save()
                    count+=1

        users = User.objects.exclude(email__exact=Lower(F('email')))
        for u in users:
            u.email = u.email.lower()
            u.save()

        users = User.objects.filter(email__exact='')
        for u in users:
            u.delete()
