# -*- coding: utf-8 -*-
from __future__ import print_function

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


User = get_user_model()


def clean_str(obj):
    return str(obj).replace('"', '""')


class Command(BaseCommand):
    help = 'Export users data in a csv file'

    def handle(self, *files, **options):
        with open('users.csv', 'w') as csv:
            csv.write('id, biography, name, email, occupation, image, gender, gender_other, sexual_orientation, race, birth_date, phone_number, phone_number_other, address, address_number, address_extra, neighborhood, zipcode, city, state, country, social_facebook, social_twitter, social_skype, social_google, site, political_front, political_front_other, studies, school, activism_school_workplace, activism_school_workplace_other, has_mandate, mandate, ujs_manager_role, has_vote, vote_city, ujs_affiliate, from_import, paper_form, event, cpf, activism_place, origin, date_joined, last_login, \n')

            for user in User.objects.all():
                # if user.last_login is not None:
                #     import ipdb; ipdb.set_trace()
                d = '"{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}"\n'.format(clean_str(user.id),
                                                            clean_str(user.biography),
                                                            clean_str(user.name),
                                                            clean_str(user.email),
                                                            clean_str(user.occupation),
                                                            clean_str(user.image),
                                                            clean_str(user.gender),
                                                            clean_str(user.gender_other),
                                                            clean_str(user.sexual_orientation),
                                                            clean_str(user.race),
                                                            clean_str(user.birth_date),
                                                            clean_str(user.phone_number),
                                                            clean_str(user.phone_number_other),
                                                            clean_str(user.address),
                                                            clean_str(user.address_number),
                                                            clean_str(user.address_extra),
                                                            clean_str(user.neighborhood),
                                                            clean_str(user.zipcode),
                                                            clean_str(user.city),
                                                            clean_str(user.state),
                                                            clean_str(user.country),
                                                            clean_str(user.social_facebook),
                                                            clean_str(user.social_twitter),
                                                            clean_str(user.social_skype),
                                                            clean_str(user.social_google),
                                                            clean_str(user.site),
                                                            clean_str(user.political_front),
                                                            clean_str(user.political_front_other),
                                                            clean_str(user.studies),
                                                            clean_str(user.school),
                                                            clean_str(user.activism_school_workplace),
                                                            clean_str(user.activism_school_workplace_other),
                                                            clean_str(user.has_mandate),
                                                            clean_str(user.mandate),
                                                            clean_str(user.ujs_manager_role),
                                                            clean_str(user.has_vote),
                                                            clean_str(user.vote_city),
                                                            clean_str(user.ujs_affiliate),
                                                            clean_str(user.from_import),
                                                            clean_str(user.paper_form),
                                                            clean_str(user.event),
                                                            clean_str(user.cpf),
                                                            clean_str(user.activism_place),
                                                            clean_str(user.origin),
                                                            clean_str(user.date_joined),
                                                            clean_str(user.last_login))
                csv.write(d)
