from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _

from allauth.socialaccount.models import SocialAccount
from cities_light.models import City, Region, Country

import hashlib


class PoliticalFront(models.Model):
    name = models.CharField(
        _('Name'),
        max_length=255,
        unique=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Political Front')
        verbose_name_plural = _('Political Fronts')

class ActivismSchoolWorkplace(models.Model):
    name = models.CharField(
        _('Name'),
        max_length=255,
        unique=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Activism School Workplace')
        verbose_name_plural = _('Activism School Workplaces')


class Event(models.Model):
    name = models.CharField(
        _('Name'),
        max_length=255,
        unique=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')


class UjsManagerRole(models.Model):
    name = models.CharField(
        _('Name'),
        max_length=255,
        unique=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('UJS Manager Role')
        verbose_name_plural = _('UJS Manager Roles')


class User(AbstractUser):

    RACE_CHOICES = (
        ('BLACK', _('Black')),
        ('BROWN', _('Brown')),
        ('WHITE', _('White')),
        ('YELLOW', _('Yellow')),
        ('INDIGENOUS', _('Indigenous')),
        ('DO_NOT_KNOW', _('Do not know')),
        ('UNDECLARED', _('Undeclared')),
    )

    GENDER_CHOICES = (
        ('CIS_FEMALE', _('Cis Female')),
        ('CIS_MALE', _('Cis Male')),
        ('TRANSSEXUAL_WOMAN', _('Transsexual woman')),
        ('TRANSSEXUAL_MAN', _('Transsexual man')),
        ('NON-BINARY', _('Non binary')),
        ('TRANSGENDER', _('Transgender')),
        ('OTHER', _('Other')),

        ('AGENDER', _('Agender')),
        ('GENDERQUEER', _('Genderqueer')),
        ('GENDERFLUID', _('Genderfluid')),
        ('VARIANT_GENDER', _('Variant gender')),
        ('PANGENDER', _('Pangender')),
        ('TRANSFEMINAL', _('Transfeminal')),
        ('NONE', _('None')),
        ('FEMALE', _('Female')),
        ('MALE', _('Male')),

    )

    SEXUAL_ORIENTATION_CHOICES = (
        ('LESBICA', _('Lesbian')),
        ('GAY', _('Gay')),
        ('BISSEXUAL', _('Bissexual')),
        ('HETEROSSEXUAL', _('Heterossexual')),
        ('ASSEXUAL', _('Assexual')),
        ('FLUIDO', _('Fluid')),
        ('OTHER', _('Other'))
    )

    ORIGIN_CHOICES = (
        ('ocupa', _('Home Ocupa')),
        ('manu', _(u'Home Manuela D\'Ávila')),
        ('donation', _(u'Doação Ocupa')),
        ('affiliation', _(u'Filiação')),
        ('signup', _(u'Cadastro Normal')),
    )

    ACTIVISM_PLACE_CHOICES = (
        ('residential_area', 'Local de moradia'),
        ('school', 'Local de estudo'),
        ('workplace', 'Local de trabalho'),
    )

    # Personal info
    biography = models.TextField(_('Biography'), blank=True, null=True)
    name = models.CharField(
        _('Name'),
        blank=True,
        max_length=255,
        null=True
    )
    occupation = models.CharField(
        _('Occupation'),
        null=True,
        blank=True,
        max_length=255
    )
    image = models.ImageField(
        _('Image'),
        blank=True,
        null=True,
        upload_to='profile_images'
    )
    gender = models.CharField(
        _('Gender'),
        null=True,
        choices=GENDER_CHOICES,
        max_length=255,
        blank=True
    )
    gender_other = models.CharField(
        _('Other type of gender'),
        null=True, max_length=255,
        blank=True
    )
    sexual_orientation = models.CharField(
        _('Sexual orientation'),
        null=True, max_length=255,
        # choices=SEXUAL_ORIENTATION_CHOICES,
        blank=True
    )
    sexual_orientation_other = models.CharField(
        _('Other sexual orientation'),
        null=True, max_length=255,
        blank=True
    )
    race = models.CharField(
        _('Race'),
        null=True,
        blank=True,
        choices=RACE_CHOICES,
        max_length=255
    )
    birth_date = models.DateField(
        _('Birth Date'),
        blank=True,
        null=True
    )

    # Contact info
    phone_number = models.CharField(
        _('Phone number'),
        null=True,
        blank=True,
        max_length=255
    )
    phone_number_other = models.CharField(
        _('Other phone number'),
        null=True,
        blank=True,
        max_length=255
    )

    # Localization info
    address = models.CharField(
        _('Address'),
        null=True, max_length=255,
        blank=True
    )
    address_number = models.CharField(
        _('Address number'),
        null=True, max_length=255,
        blank=True
    )
    address_extra = models.CharField(
        _('Extra information for address'),
        null=True, max_length=255,
        blank=True
    )
    neighborhood = models.CharField(
        _('Neighborhood'),
        null=True, max_length=255,
        blank=True
    )
    zipcode = models.CharField(
        _('ZIP Code'),
        null=True, max_length=255,
        blank=True
    )
    city = models.ForeignKey(City, verbose_name=_('City'), related_name='users', null=True, blank=True)
    state = models.ForeignKey(Region, verbose_name=_('Region'), related_name='users', null=True, blank=True)
    country = models.ForeignKey(Country, verbose_name=_('Country'), related_name='users', null=True, blank=True)

    # Social info
    social_facebook = models.CharField(
        _('Facebook'),
        null=True, max_length=255,
        blank=True
    )
    social_twitter = models.CharField(
        _('Twitter'),
        null=True, max_length=255,
        blank=True
    )
    social_skype = models.CharField(
        _('Skype'),
        null=True, max_length=255,
        blank=True
    )
    social_google = models.CharField(
        _('Google'),
        null=True, max_length=255,
        blank=True
    )
    site = models.CharField(
        _('Site'),
        null=True, max_length=255,
        blank=True
    )

    # Political info
    political_front = models.ManyToManyField(
        PoliticalFront,
        verbose_name=_('Political front'),
        related_name='users',
        blank=True
    )
    political_front_other = models.CharField(
        _('Other political front'),
        null=True, max_length=255,
        blank=True
    )
    studies = models.NullBooleanField(
        _('Studies'),
        blank=True
    )
    school = models.CharField(
        _('School'),
        null=True, max_length=255,
        blank=True
    )
    activism_school_workplace = models.ManyToManyField(
        ActivismSchoolWorkplace,
        verbose_name=_('Activism in school and/or workplace'),
        related_name='users',
        blank=True
    )
    activism_school_workplace_other = models.CharField(
        _('Other activism form in school and/or workplace'),
        null=True, max_length=255,
        blank=True
    )
    has_mandate = models.NullBooleanField(
        _('Has a mandate'),
        blank=True
    )
    mandate = models.CharField(
        _('Mandate'),
        null=True, max_length=255,
        blank=True
    )
    is_working = models.NullBooleanField(
        _('Is Working'),
        blank=True
    )
    workplace = models.CharField(
        _('Workplace'),
        null=True, max_length=255,
        blank=True
    )
    ujs_manager_role = models.ManyToManyField(
        UjsManagerRole,
        verbose_name=_('UJS management role'),
        related_name='users',
        blank=True
    )
    has_vote = models.NullBooleanField(
        _('Has vote'),
        blank=True
    )
    vote_city = models.CharField(
        _('Vote city'),
        null=True, max_length=255,
        blank=True
    )

    # Affiliation info
    ujs_affiliate = models.NullBooleanField(
        _('UJS affiliate'),
        blank=True
    )
    from_import = models.NullBooleanField(
        _('This user came from a bulk import'),
        blank=True
    )
    paper_form = models.NullBooleanField(
        _('Affiliation made in a paper form'),
        blank=True
    )
    accepted_terms = models.NullBooleanField(
        _('User accepted terms'),
        blank=True
    )
    accepted_statute = models.NullBooleanField(
        _('User accepted the statute'),
        blank=True
    )
    is_able_to_contribute = models.NullBooleanField(
        _('User is able to contribute with monthly donation'),
        blank=True
    )

    # Participation info
    event = models.ManyToManyField(
        Event,
        verbose_name=_('Event'),
        related_name='users',
        blank=True
    )

    cpf = models.CharField(
        _('CPF'),
        max_length=11,
        null=True,
        blank=True
    )

    activism_place = models.CharField(
        _('Activism place'),
        null=True,
        choices=ACTIVISM_PLACE_CHOICES,
        max_length=30,
        blank=True
    )

    origin = models.CharField(
        _('Subscription origin'),
        null=True,
        choices=ORIGIN_CHOICES,
        max_length=20,
        blank=True
    )

    email = models.EmailField(_('email address'), unique=True)

    congress_subscription = models.BooleanField(_('User is subscribed to congress'), default=False)
    congress_subscription_in_progress = models.BooleanField(_('User has started subscription'), default=False)
    congress_subscription_date = models.DateTimeField(_('Subscription date'), null=True)

    def __str__(self):
        return self.email

    @property
    def image_url(self):
        if self.image:
            return self.image.url
        social_accounts = SocialAccount.objects.filter(user_id=self.id)

        for account in social_accounts:
            picture = account.get_avatar_url()
            if picture:
                return picture
        return "https://gravatar.com/avatar/{}?s=40&d=mm".format(hashlib.md5(self.email.encode('utf-8')).hexdigest())

    def get_full_name(self):
        return self.name


    def save(self, *args, **kwargs):
        is_new = self.pk is None
        group = None
        if Group.objects.filter(name='Forum').exists():
            group = Group.objects.get(name='Forum')

        self.email = self.email.lower()
        super().save(*args, **kwargs)

        if is_new and group:
            try:
                self.groups.add(group)
                self.save()
            except Group.DoesNotExist:
                pass

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
