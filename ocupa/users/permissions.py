from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from rest_framework import permissions

User = get_user_model()


class IsCurrentUserOrAdmin(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user:
            if request.user.is_superuser:
                return True
            else:
                return obj == request.user
        else:
            return False

class IsCurrentUserAdminOrInactiveUser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user and not isinstance(request.user, AnonymousUser) :
            if request.user.is_superuser or obj == request.user:
                return True
            else:
                return False
        else:
            try:
                user = User.objects.get(email=request.data['email'])
                if user.from_import and not user.has_usable_password():
                    return True
                else:
                    return False
            except User.DoesNotExist:
                return False
