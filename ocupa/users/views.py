# -*- coding: utf-8 -*-
import requests
from datetime import datetime
from allauth.account.models import EmailAddress
from allauth.account.utils import send_email_confirmation
from allauth.socialaccount.models import SocialApp
from cities_light.models import Region, City
from django.contrib.admin.models import LogEntry, CHANGE
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode as uid_decoder
from django.utils.translation import ugettext_lazy as _
from django.http import Http404, JsonResponse
from django.conf import settings
from requests_oauthlib import OAuth1
from rest_framework.permissions import AllowAny
from rest_framework.decorators import detail_route, parser_classes, list_route
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework import permissions, viewsets, generics, mixins, views
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from allauth.account.utils import setup_user_email
from rest_auth.views import LoginView, PasswordResetView, \
    PasswordResetConfirmView
from rest_auth.registration.views import SocialLoginView, SocialConnectView
from rest_auth.social_serializers import TwitterLoginSerializer, \
    TwitterConnectSerializer
from urllib.parse import parse_qs

from courier.emails.serializers import EmailProfileSerializer
from courier.emails.models import EmailProfile
from ocupa.donation.views import get_valid_username
from ocupa.users.models import User, PoliticalFront, ActivismSchoolWorkplace, UjsManagerRole
from ocupa.users.permissions import IsCurrentUserOrAdmin

from ocupa.users.serializers import (UserSerializer, FixSocialLoginSerializer,
                          PoliticalFrontSerializer, SimpleUserSerializer,
                          ActivismSchoolWorkplaceSerializer,
                          UjsManagerRoleSerializer, RegistrationSerializer,
                          SimpleUserRegistrationSerializer)

from ocupa.users.filters import UserStatsFilterBackend
from ocupa.users.serializers import UserStatsSerializer

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (AllowAny,)

    @detail_route(methods=['POST'])
    @parser_classes((FormParser, MultiPartParser))
    def image(self, request, *args, **kwargs):
        if 'image' in request.data:
            user_profile = self.get_object()
            user_profile.image.delete()

            upload = request.data['image']

            user_profile.image.save(upload.name, upload)

            return Response(status=status.HTTP_201_CREATED,
                            headers={'Location': user_profile.image.url})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def user_status(self, request):
        try:
            email = request.data.get('email', None)
            if email:
                user = User.objects.get(email=email)
                if 'congress_subscription' in request.data:
                    user.congress_subscription_in_progress = True
                    user.save()
                return Response({'user': email,
                                 'active': user.has_usable_password() or
                                           user.socialaccount_set.count() > 0,
                                 'exists':  True,
                                 'ujs_affiliate': user.ujs_affiliate},
                                status=status.HTTP_200_OK)
            else:
                return Response({'error': 'e-mail field required'},
                                status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({
                'user': request.data['email'],
                'active': False,
                'exists': False,
                'ujs_affiliate': False
            }, status=status.HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def register_user(self, request):
        captcha_valid = self.validate_captcha(request.data.get('token', ''))
        if not captcha_valid:
            return Response({
                'error': True,
                'message': 'invalid captcha'
            }, status=status.HTTP_200_OK)

        user_data = request.data.get('user', None)
        if not user_data:
            return Response({
                'error': True,
                'message': 'incomplete data set'
            }, status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(email=user_data['email']).exists():
            user = User.objects.get(email=user_data['email'])
            if not user.zipcode:
                user.zipcode = user_data['zipcode']
            if not user.origin:
                user.origin = user_data['origin']
            if not user.city and user_data.get('city', None):
                user.city = City.objects.get(pk=user_data['city'])
            if not user.state and user_data.get('state', None):
                user.state = Region.objects.get(pk=user_data['state'])
            user.save()

            return Response({
                'error': True,
                'message': 'user already subscribed'
            }, status=status.HTTP_200_OK)

        user_data['username'] = get_valid_username(user_data['email'])

        if user_data.get('origin', None) and \
                user_data['origin'] in ['ocupa', 'manu']:
            serializer = SimpleUserRegistrationSerializer(data=user_data,
                                                    context={
                                                        'request': request
                                                    })
            if serializer.is_valid():
                serializer.save()
                return Response({
                    'error': False,
                    'data': serializer.data
                    }, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors,
                                status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                'error': True,
                'message': 'no origin sent'
            }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['GET'], permission_classes=[IsCurrentUserOrAdmin])
    def me(self, request):
        if self.request.user.id is None:
            raise Http404
        try:
            email_profile = self.request.user.mailing_profile.get()
        except EmailProfile.DoesNotExist:
            email_profile = EmailProfile.objects.create(user=self.request.user)

        eps = EmailProfileSerializer(email_profile)
        serializer = UserSerializer(self.request.user)
        data = serializer.data
        data['mailing_profile'] = eps.data
        return Response(data)

    def update(self, request, *args, **kwargs):
        message = self.get_change_message()
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.request.user = serializer.save()
        if not self.request.user.congress_subscription_date:
            self.request.user.congress_subscription_date = datetime.now()
            self.request.user.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        data = serializer.data
        if EmailProfile.objects.filter(user=self.request.user).exists():
            eps = EmailProfileSerializer(self.request.user.mailing_profile.get())
            data['mailing_profile'] = eps.data

        LogEntry.objects.log_action(
            user_id         = request.user.pk,
            content_type_id = ContentType.objects.get_for_model(self.get_object()).pk,
            object_id       = self.get_object().pk,
            object_repr     = str(self.get_object()),
            action_flag     = CHANGE,
            change_message  = message
        )

        return Response(data)

    def create(self, request, *args, **kwargs):
        captcha_valid = self.validate_captcha(request.data.get('token', ''))
        if not captcha_valid:
            return Response({
                'captcha': ['invalid captcha']
            }, status=status.HTTP_400_BAD_REQUEST)

        return super(UserViewSet, self).create(request, *args, **kwargs)

    def get_changed(self):
        sent_data = self.request.data
        Serializer = self.get_serializer_class()
        obj = Serializer(self.get_object())
        instance = obj.data
        changed = []
        for key in sent_data:
            if key in instance and not(instance[key] == sent_data[key]):
                changed.append(key)

        return changed

    def get_change_message(self):
        changed = self.get_changed()
        if len(changed):
            message = 'Alterado {0}'.format(', '.join(changed))
        else:
            message = u''
        return message

    def get_permissions(self):
        if self.action == 'list':
            self.permission_classes = [permissions.IsAdminUser, ]
        elif self.action == 'retrieve':
            self.permission_classes = [IsCurrentUserOrAdmin]
        elif self.action == 'update':
            self.permission_classes = [IsCurrentUserOrAdmin]
        return super(UserViewSet, self).get_permissions()

    def validate_captcha(self, captcha_response):
        captcha_req = requests\
            .post('https://www.google.com/recaptcha/api/siteverify',
                  data = {
                      'secret': settings.RECAPTCHA_SECRET_KEY,
                      'response': captcha_response
                  })
        return captcha_req.json()['success']


class UserAffiliationView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        if User.objects.filter(username=request.data['username']).exists():
            user = User.objects.get(username=request.data['username'])
            if user.ujs_affiliate:
                errors = {'non_field_errors' : [_('User is already affiliated')]}
            elif user.has_usable_password() or user.socialaccount_set.count() > 0:
                errors = {'email': [_('A user is already registered with this e-mail address.')]}
            else:
                errors = None
            if errors:
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            request.data['captcha'] = request.data['token']
            register = RegistrationSerializer(data=request.data)
            if register.is_valid():
                user = register.save(request)
                send_email_confirmation(request, user)
            else:
                return Response(register.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer = UserSerializer(data=request.data, instance=user)
        if serializer.is_valid():
            user = serializer.save()
            if user.congress_subscription:
                user.congress_subscription_date = datetime.now()
                user.congress_subscription_in_progress = False

            eps, created = EmailProfile.objects.get_or_create(user=user)
            eps.active = request.data['mailing_profile']['active']
            eps.save()
            user.save()

            # FIXME send mail to warn user
            if not user.has_usable_password() and user.socialaccount_set.count() == 0:
                send_email_confirmation(request, user)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status=status.HTTP_200_OK)


class StatsViewSet(mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = User.objects.all()
    filter_backends = (UserStatsFilterBackend,)
    serializer_class = UserStatsSerializer
    permission_classes = (permissions.IsAdminUser,)

    def get_serializer(self, *args, **kwargs):
        serializer = self.get_serializer_class()
        if not ('per_state' in self.request.query_params):
            return serializer(*args)
        return serializer(*args, **kwargs)


class SimpleUserViewSet(generics.GenericAPIView, mixins.RetrieveModelMixin):
    serializer_class = SimpleUserSerializer
    queryset = User.objects.all()
    lookup_field = ('username', 'email')


class PoliticalFrontViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PoliticalFrontSerializer
    queryset = PoliticalFront.objects.all()


class ActivismSchoolWorkplaceViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ActivismSchoolWorkplaceSerializer
    queryset = ActivismSchoolWorkplace.objects.all()


class UjsManagerRoleViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UjsManagerRoleSerializer
    queryset = UjsManagerRole.objects.all()


# FIXME: the social classes may need rework when updating to v0.9.3
#        FixSocialLoginSerializer class may not be needed anymore
class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
    serializer_class = FixSocialLoginSerializer


class TwitterLogin(LoginView):
    serializer_class = TwitterLoginSerializer
    adapter_class = TwitterOAuthAdapter


class FacebookConnect(SocialConnectView):
    adapter_class = FacebookOAuth2Adapter


class TwitterConnect(SocialConnectView):
    serializer_class = TwitterConnectSerializer
    adapter_class = TwitterOAuthAdapter


class TwitterRequestToken(generics.GenericAPIView):
    def post(self, request):
        if not SocialApp.objects.filter(provider='twitter').exists():
            return Response({'message': _('No Twitter provider found')},
                            status=status.HTTP_404_NOT_FOUND)
        twitter_app = SocialApp.objects.get(provider='twitter')
        CONSUMER_KEY = twitter_app.client_id
        CONSUMER_SECRET = twitter_app.secret

        callback = request\
            .build_absolute_uri(reverse('twitter_connect_callback'))
        auth = OAuth1(CONSUMER_KEY, CONSUMER_SECRET, callback_uri=callback)
        r = requests.post('https://api.twitter.com/oauth/request_token',
                          auth=auth)
        if r.status_code == 200:
            data = parse_qs(r.content.decode('utf-8'))
        else:
            return Response({'message': r.content}, status.HTTP_400_BAD_REQUEST)

        return Response(data, status=status.HTTP_200_OK)


class TwitterAccessToken(generics.GenericAPIView):
    def post(self, request):
        if not SocialApp.objects.filter(provider='twitter').exists():
            return Response({'error': _('No Twitter provider found')},
                            status=status.HTTP_404_NOT_FOUND)
        twitter_app = SocialApp.objects.get(provider='twitter')
        CONSUMER_KEY = twitter_app.client_id
        CONSUMER_SECRET = twitter_app.secret

        auth = OAuth1(CONSUMER_KEY, CONSUMER_SECRET,
                      request.data['oauth_token'],
                      verifier=request.data['oauth_verifier'])
        r = requests\
            .post('https://api.twitter.com/oauth/access_token', auth=auth)
        if r.status_code == 200:
            data = parse_qs(r.content.decode('ascii'))
        else:
            return Response({'message': r.content}, status.HTTP_400_BAD_REQUEST)

        return Response(data, status=status.HTTP_200_OK)


def get_api_key(request):
    if request.user.id is None:
            raise Http404

    token = Token.objects.get_or_create(user=request.user)
    return JsonResponse({'key': token[0].key}, status=200)


class OcupaLoginView(LoginView):

    def post(self, request, *args, **kwargs):
        email = request.data.get('email', None).lower()
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return Response({'detail': 'Usuário não encontrado'},
                            status=status.HTTP_400_BAD_REQUEST)

        if not user.has_usable_password():
            return Response({
                'non_field_errors' : ['User is inactive'],
            }, status=status.HTTP_400_BAD_REQUEST)

        EmailAddress.objects.filter(user=user).exclude(email=email).delete()
        has_verifiable_email = EmailAddress.objects.filter(user=user).exists()
        has_verified_email = EmailAddress.objects.filter(user=user, verified=True).exists()
        if not has_verified_email:
            if not has_verifiable_email:
                setup_user_email(request, user, [])
            send_email_confirmation(request, user)
            return Response({
                'non_field_errors' : ['Por favor verifique seu e-mail'],
            }, status=status.HTTP_400_BAD_REQUEST)

        self.request = request
        self.serializer = self.get_serializer(data=self.request.data,
                                              context={'request': request})
        if (self.serializer.is_valid()):
            self.login()
            return self.get_response()
        else:
            return Response(self.serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class OcupaPasswordResetView(PasswordResetView):

    def post(self, request, *args, **kwargs):
        email = request.data.get('email', None)
        try:
            user = User.objects.get(email=email)
            if not user.has_usable_password():
                user.set_password(User.objects.make_random_password())
                user.save()

            has_verifiable_email = EmailAddress.objects \
                .filter(user=user) \
                .exists()

            if not has_verifiable_email:
                email_address = user.emailaddress_set \
                    .add_email(request, user, user.email)
                email_address.set_as_primary()
        except:
            pass

        # Create a serializer with request.data
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            # Return the success message with OK HTTP status
            return Response({
                "detail": "Password reset e-mail has been sent."
            }, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class OcupaPasswordResetConfirmView(PasswordResetConfirmView):

    def post(self, request, *args, **kwargs):
        uid = request.data.get('uid', None)
        try:
            uid = force_text(uid_decoder(uid))
            user = User.objects.get(pk=uid)

            has_verified_email = EmailAddress.objects\
                .filter(user=user, verified=True)\
                .exists()

            if not has_verified_email:
                email_address = EmailAddress.objects \
                    .get(user=user, email=user.email, verified=False)
                email_address.verified = True
                email_address.save()
        except:
            pass

        return super(OcupaPasswordResetConfirmView, self)\
            .post(request, *args, **kwargs)


