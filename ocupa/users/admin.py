import datetime
from allauth.account.models import EmailAddress
from cities_light.models import City, Region
from dal import autocomplete
from django import forms
from django.contrib import admin
from django.contrib.admin import RelatedOnlyFieldListFilter
from django.contrib.admin.models import LogEntry
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.files.base import ContentFile
from django.db.models import Q, Count, Prefetch
from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, gettext

from .models import PoliticalFront, ActivismSchoolWorkplace, UjsManagerRole, Event
from django.conf.urls import url

User = get_user_model()


class AffiliationPermissionsMixin(object):
    """
    Defines and enforces access and modification restricitons for certain users.
    Restrictions will only be aplied if they are defined for the current users.
    If no state restrictions are defined, the user will be able to see and edit
    any user (even if he isn't a superuser)
    """

    def get_allowed_states(self, user):
        allowed_states = []

        affiliation_groups = user.groups.filter(name__contains='Secretaria-Organizacao_').values_list('name')
        if affiliation_groups:
            # Group's names are expected to be in the format Secretaria-Organizacao_<STATE>
            # The following line extracts the state's name
            allowed_states = [x[0].split('_')[1] for x in affiliation_groups]

        return allowed_states

    # Filter wich user entries will be available for edition depending on the user
    def get_queryset(self, request):
        queryset = super().get_queryset(request)

        user = request.user
        allowed_states = self.get_allowed_states(user)

        if allowed_states and not user.is_superuser:
            queryset = queryset.filter(
                (Q(state__slug__in=allowed_states) | Q(state=None)) &
                Q(is_superuser=False, is_staff=False)
            )

        return queryset

    # Filter wich fields will be visible depending on the user
    def get_fieldsets(self, request, obj=None):
        """
        Hook for specifying fieldsets.
        """

        fieldsets = super().get_fieldsets(request, obj)

        # Check current user's permissions level to decide wich fields to show
        # Superuser status and group relationships are the criteria here
        # A superuser should always view any fields
        user = request.user

        # Hide info from 'Informações pessoais' section from everybody
        try:
            fieldsets[1][1]['fields'] = tuple(x for x in fieldsets[1][1]['fields'] if x != 'first_name' and x != 'last_name')
        except IndexError:
            pass

        # If the user can only see people from specific states, hide more fields
        if self.get_allowed_states(user) and not user.is_superuser and obj:
            try:
                # Hide 'Permissões' section
                fieldsets = tuple(x for x in fieldsets if x[0] != 'Permissões')

                # Hide username and password section
                # fieldsets = tuple(x for x in fieldsets if x[0] is not None)
            except IndexError:
                # The form been edited problably is the user creation one. Pass this for now
                pass

        return fieldsets

    # Filter the options of form fields depending on the user
    def get_form(self, request, obj=None, **kwargs):
        form = super(AffiliationPermissionsMixin, self).get_form(request, obj, **kwargs)

        user = request.user
        allowed_states = self.get_allowed_states(user)
        form.base_fields['state'].queryset = Region.objects.all()
        form.base_fields['city'].queryset = City.objects.all()
        try:
            if allowed_states and user.is_staff:
                form.base_fields['state'].queryset = form.base_fields['state'].queryset.filter(slug__in=allowed_states)
                form.base_fields['city'].queryset = form.base_fields['city'].queryset.filter(region__slug__in=allowed_states)
        except KeyError:
            pass

        return form


class MyUserChangeForm(UserChangeForm):

    city = forms.ModelChoiceField(queryset=City.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='cities:city-autocomplete', forward=['state']))

    def __init__(self, *args, **kwargs):
        super(MyUserChangeForm, self).__init__(*args, **kwargs)

        self.fields['name'].required = True
        self.fields['phone_number'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True

    class Meta(UserChangeForm.Meta):
        model = User

    def clean_password(self):
        # Since its possible that the password field wasn't visible on the form
        # (in case that the user doesn't have sufficioente permissions),
        # bypass its validation here
        try:
            return self.initial["password"]
        except KeyError:
            return None

    def save(self, commit=True):
        user = super().save(commit=False)
        if not (user.email == user.username):
            username = user.email.lower()
            i = 1
            while User.objects.filter(username=username).exists() and \
                not(User.objects.get(username=username).id == user.id):
                _email = username.split('@')
                _email[0] = _email[0] + '+{}'.format(str(i))
                username = '@'.join(_email)

            user.username = username

        if commit:
            user.save()

        return user


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': _('This username has already been taken.')
    })

    # A password should not be requested or validated during user creation
    password2 = None
    password1 = None
    email = forms.EmailField()
    paper_form = forms.BooleanField(initial=True, widget=forms.HiddenInput)
    name = forms.CharField(max_length=255, widget=forms.TextInput)
    phone_number = forms.CharField(max_length=30)
    state = forms.ModelChoiceField(queryset=Region.objects.all())
    city = forms.ModelChoiceField(queryset=City.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='cities:city-autocomplete', forward=['state']))


    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'email', 'paper_form', 'name', 'state', 'city')

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)

        try:
            User.objects.get(email=self.cleaned_data['email'])
        except User.DoesNotExist:
            # Since this is a new user, prepare a username for him
            username = user.email.lower()
            i = 1
            while User.objects.filter(username=username).exists():
                _email = username.split('@')
                _email[0] = _email[0] + '+{}'.format(str(i))
                username = '@'.join(_email)

            user.username = username

        if commit:
            user.save()

        return user


class VerificationEmailFilter(admin.SimpleListFilter):
    title = _('Verified E-mail')

    parameter_name = 'emailaddress'

    def lookups(self, request, model_admin):
        return (
            ('verified', _('Verified')),
            ('not_verified', _('Not Verified')),
            ('no_mail', _('Does not have a verification E-mail'))
        )

    def queryset(self, request, queryset):
        if self.value() == 'no_mail':
            return queryset.annotate(count_mail=Count('emailaddress'))\
                .filter(count_mail=0)
        elif self.value() == 'verified':
            return queryset.filter(emailaddress__verified=True)
        elif self.value() == 'not_verified':
            return queryset.filter(emailaddress__verified=False)

        return queryset


class RelatedOnlyDropdownListFilter(RelatedOnlyFieldListFilter):
    template = "admin/dropdown_filter.html"


@admin.register(User)
class MyUserAdmin(AffiliationPermissionsMixin, AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm

    fieldsets = (
        (None, {'fields': ('password',)}),
        (_('Personal info'), {'fields': (
                'name', 'email', 'race', 'gender', 'gender_other',
                'sexual_orientation', 'sexual_orientation_other', 'is_working', 'workplace',
                'occupation', 'biography', 'birth_date', 'image', 'cpf'
            )}),

        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),

        (_('Contact info'), {'fields': (
                'phone_number', 'phone_number_other',
            )}),
        (_('Localization info'), {'fields': (
                'address', 'address_number', 'address_extra',
                'zipcode', 'state', 'city', 'country',
            )}),
        (_('Social info'), {'fields': (
                'social_facebook', 'social_twitter',
                'social_skype', 'social_google', 'site',
            )}),
        (_('Political info'), {'fields': (
                'political_front', 'political_front_other', 'studies',
                'school', 'activism_school_workplace', 'activism_school_workplace_other',
                'has_mandate', 'mandate', 'ujs_manager_role', 'has_vote', 'vote_city'
            )}),
        (_('Affiliation info'), {'fields': (
                'ujs_affiliate', 'from_import', 'paper_form',
                'is_able_to_contribute', 'accepted_terms', 'accepted_statute'
            )}),
        (_('Participation info'), {'fields': (
                'event', 'congress_subscription', 'congress_subscription_in_progress',
                'congress_subscription_date'
            )}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'paper_form', 'name', 'phone_number', 'state',
                       'city',),
        }),
    )

    list_display = ('name', 'email', 'phone_number', 'state', 'last_login', 'date_joined',
                    'last_log_entry')
    list_display_links = ('email',)
    readonly_fields = ('last_log_entry', 'congress_subscription_date')
    list_filter = [('state', RelatedOnlyDropdownListFilter), 'last_login',
                   'congress_subscription', 'from_import', 'paper_form', 'ujs_affiliate',
                   'is_staff', 'is_superuser', ('city', RelatedOnlyDropdownListFilter),
                   ('political_front', RelatedOnlyDropdownListFilter),
                   VerificationEmailFilter, 'date_joined']
    search_fields = ['id', 'name', 'email']
    actions = ['update_email_address']
    ordering = ('-date_joined',)

    def get_urls(self):
        urls = super(MyUserAdmin, self).get_urls()
        my_urls = [
            url(r'^download/$',
                self.admin_site.admin_view(self.download_csv),
                name='download_users_database'),
            url(r'^download/mailing_list$',
                self.admin_site.admin_view(self.download_mailing_csv),
                name='download_domain_stats'),
            url(r'^download/congress/$',
                self.admin_site.admin_view(self.download_congress),
                name='download_congress')
        ]
        return my_urls + urls

    def download_mailing_csv(self, request):
        users = User.objects \
            .select_related('city') \
            .select_related('state') \
            .prefetch_related(
            Prefetch('political_front',
                     queryset=PoliticalFront.objects.all(),
                     to_attr='political_front_list'))

        verified = list(EmailAddress.objects\
            .filter(verified=True)\
            .values_list('user_id', flat=True))

        rendered = 'email, name, phone_number, django_id, ' \
                   'city, state, from_import, ujs_affiliate, ' \
                   'political_front, verified, last_login, date_joined\r\n'

        exclude_contains = [',', '%', 'ã', '?', ' ', 'á', 'é', 'í', 'ô', '...',
                            '(', 'ç', '.@', 'º', '%', '#', '&', ']', ':', '/',
                            '!', '..', ';', '´', '\\', '\'', 'ó', 'ê', '@_',
                            '@.', '>', 'â', '³', '@-', '"']

        exclude_start = ['abuse@', 'admin@', 'billing@', 'compliance@', 'devnull@',
                         'dns@', 'ftp@', 'hostmaster@', 'inoc@', 'ispfeedback@',
                         'ispsupport@', 'list-request@', 'list@', 'maildaemon@',
                         'noc@', 'no-reply@', 'noreply@', 'null@', 'phish@',
                         'phishing@', 'postmaster@', 'privacy@', 'registrar@',
                         'root@', 'security@', 'spam@', 'support@', 'sysadmin@',
                         'tech@', 'undisclosed-recipients@', 'unsubscribe@',
                         'abc@', '123@', '1234@',  'xxx@', 'none@', 'abcd@',
                         'usenet@', 'uucp@' 'webmaster@', 'www@', '.', ]

        single_number = ['%s@' % (chr(x),) for x in range(48,58)]
        single_letter = ['%s@' % (chr(x),) for x in range(97,123)]

        exclude_start.extend(single_number)
        exclude_start.extend(single_letter)

        exclude_domains = []
        domains = staticfiles_storage.open('data/invalid_mail_domains')
        lines = domains.readlines()
        exclude_domains = [l.strip(b'\n').decode('utf-8') for l in lines]

        exclude_end = ['1', '2', '.', '.c0m']

        for e in exclude_domains:
            users = users.exclude(email__endswith=e)

        users = users.exclude(email='')

        for e in exclude_contains:
            users = users.exclude(email__icontains=e)

        for e in exclude_start:
            users = users.exclude(email__istartswith=e)

        for e in exclude_end:
            users = users.exclude(email__iendswith=e)

        items = []
        users = list(users.order_by('id'))
        for u in users:
            row = [
                u.email, u.name or '', u.phone_number or '', u.id,
                u.city and u.city.name or '', u.state and u.state.name or '',
                u.from_import or False, u.ujs_affiliate or False,
                ','.join([pf.name for pf in u.political_front_list]),
                u.id in verified, u.last_login or '', u.date_joined
            ]
            l_map = map(str, row)
            items.append('"' + '","'.join(l_map) + '"')

        rendered += '\r\n'.join(items)
        content_file = ContentFile(bytes(rendered, 'utf-8'))
        response =  HttpResponse(content_file, 'text/csv')
        response['Content-Length']      = content_file.size
        response['Content-Disposition'] = 'attachment; filename="base_usuarios_mailchimp.csv"'

        return response

    @staticmethod
    def generate_content_file(users):
        rendered = 'name,email,id,cpf,congress_subscription,congress_subscription_date, activism_place,activism_school_workplace_other,address,\
            address_extra,address_number,biography,birth_date,city,country,\
            gender,gender_other,has_mandate,has_vote,\
            mandate,neighborhood,occupation,phone_number,\
            phone_number_other,political_front_other,race,school,\
            sexual_orientation,site,social_facebook,social_google,social_skype,\
            social_twitter,state,studies,vote_city,zipcode,paper_form,\
            from_import, ujs_affiliate,origin\r\n'

        values = users\
            .order_by('email')\
            .values_list('name', 'email', 'id', 'cpf', 'congress_subscription',
                         'congress_subscription_date', 'activism_place', 'activism_school_workplace_other',
                         'address', 'address_extra', 'address_number', 'biography',
                         'birth_date', 'city__name', 'country__name', 'gender',
                         'gender_other', 'has_mandate', 'has_vote',
                         'mandate', 'neighborhood',
                         'occupation', 'phone_number', 'phone_number_other',
                         'political_front_other', 'race', 'school',
                         'sexual_orientation', 'site', 'social_facebook',
                         'social_google', 'social_skype', 'social_twitter',
                         'state__name', 'studies', 'vote_city', 'zipcode',
                         'paper_form', 'from_import', 'ujs_affiliate', 'origin')


        def _str(model_field):
            if not model_field:
                return ''
            if isinstance(model_field, datetime.datetime):
                model_field = model_field.strftime('%d/%m/%Y %H:%M')

            return str(model_field).replace('"', '').replace('\\', '')

        items = ['"{}"'.format('","'.join(map(_str, l))) for l in list(values)]
        rendered += '\r\n'.join(items)
        return ContentFile(bytes(rendered, 'utf-8'))

    def download_csv(self, request):
        content_file = self.generate_content_file(self.get_queryset(request))
        response =  HttpResponse(content_file, 'text/csv')
        response['Content-Length']      = content_file.size
        response['Content-Disposition'] = 'attachment; filename="base_usuarios.csv"'

        return response

    def download_congress(self, request):
        users = self.get_queryset(request).filter(congress_subscription=1)
        content_file = self.generate_content_file(users)
        response =  HttpResponse(content_file, 'text/csv')
        response['Content-Length']      = content_file.size
        response['Content-Disposition'] = 'attachment; filename="base_usuarios.csv"'

        return response

    def update_email_address(self, request, queryset):
        for user in queryset:
            email_address = user.emailaddress_set\
                .add_email(request, user, user.email)
            email_address.set_as_primary()
    update_email_address.short_description = _('Update confirmation e-mails')

    def last_log_entry(self, instance):
        entry = LogEntry.objects.filter(object_id=instance.id).first()
        if entry:
            date_parts = [
                entry.action_time.strftime('%d de'),
                gettext(entry.action_time.strftime('%B')),
                entry.action_time.strftime('de %Y às %H:%M'),
            ]
            return ' '.join(date_parts)

        return mark_safe("<span class='errors'>Nenhuma alteração feita</span>")
    last_log_entry.short_description = _('Last Update')

    class Media:
        css = {
             'all': ('css/filters.css',)
        }

@admin.register(PoliticalFront)
class PoliticalFrontAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['id', 'name']


@admin.register(ActivismSchoolWorkplace)
class ActivismSchoolWorkplaceAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['id', 'name']


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['id', 'name']


@admin.register(UjsManagerRole)
class UjsManagerRoleAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['id', 'name']


class ConversationAdmin(admin.ModelAdmin):
    fields = ['author', 'title', 'description', 'dialog', 'response', 'opinion', 'polis_id',
              'comment_nudge', 'comment_nudge_interval', 'comment_nudge_global_limit',
              'background_image', 'background_color', 'polis_url', 'polis_slug',
              'position', 'is_new', 'promoted', 'category']
    list_display = ['id', 'title', 'author', 'position', 'created_at', 'updated_at']
