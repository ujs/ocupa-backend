import datetime

import requests
from cities_light.models import City, Region
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_auth.registration.serializers import SocialLoginSerializer, RegisterSerializer
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from allauth.account import app_settings as allauth_settings
from allauth.socialaccount.helpers import complete_social_login
from requests.exceptions import HTTPError

from ocupa.users.models import PoliticalFront, ActivismSchoolWorkplace, UjsManagerRole
from ocupa.donation.models import PagSeguroCheckout, PreApprovalSubscription, Campaign

User = get_user_model()


class SimpleCampaignSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = ('name', 'slug')


class DonationListFilter(serializers.ListSerializer):
    def to_representation(self, instance):
        instance = instance\
            .filter(subscription=None)\
            .exclude(status=8)\
            .order_by('-created_at')
        return super().to_representation(instance)

class UserDonationSerializer(serializers.ModelSerializer):
    amount = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    start_date = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    campaign = SimpleCampaignSerializer()

    class Meta:
        list_serializer_class = DonationListFilter
        model = PagSeguroCheckout
        fields = ('amount', 'campaign', 'start_date', 'type', 'status')

    def get_amount(self, checkout):
        return checkout.value

    def get_type(self, o):
        return 'registration' \
            if o.campaign.is_registration_process else 'donation'

    def get_start_date(self, o):
        return o.created_at

    def get_status(self, o):
        return o.status


class UserSubscriptionsSerializer(serializers.ModelSerializer):
    name = serializers.SlugRelatedField(read_only=True,
                                        slug_field='name',
                                        source='pre_approval_plan.campaign')
    end_date = serializers.SerializerMethodField()
    active = serializers.SerializerMethodField()
    campaign = SimpleCampaignSerializer(source='pre_approval_plan.campaign')
    period = serializers.SlugRelatedField(read_only=True,
                                          slug_field='period',
                                          source='pre_approval_plan')

    class Meta:
        model = PreApprovalSubscription
        fields = ('name', 'value', 'start_date', 'end_date', 'active',
                  'campaign', 'period')

    def get_active(self, o):
        return o.cancelation_date == None

    def get_end_date(self, o):
        return o.start_date + datetime.timedelta(days=365)


class UserSerializer(serializers.ModelSerializer):
    transactions = UserDonationSerializer(many=True, read_only=True,)
    subscriptions = UserSubscriptionsSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'image', 'name', 'occupation', 'email', 'biography',
                  'city', 'state', 'country', 'username', 'race', 'gender',
                  'gender_other', 'sexual_orientation', 'race', 'birth_date',
                  'sexual_orientation_other', 'phone_number', 'is_working', 'workplace',
                  'phone_number_other', 'address', 'address_number',
                  'address_extra', 'neighborhood', 'zipcode', 'social_facebook',
                  'social_twitter', 'social_skype', 'social_google', 'site',
                  'political_front', 'political_front_other', 'studies',
                  'school', 'activism_place', 'activism_school_workplace',
                  'activism_school_workplace_other', 'has_mandate', 'mandate',
                  'ujs_manager_role', 'has_vote', 'vote_city', 'cpf',
                  'is_superuser', 'ujs_affiliate', 'origin', 'transactions',
                  'subscriptions', 'is_able_to_contribute', 'accepted_terms',
                  'accepted_statute', 'congress_subscription')


    def to_internal_value(self, data):
        if 'cpf' in data:
            data['cpf'] = UserSerializer.cleanup_cpf(data['cpf'])

        return super(UserSerializer, self).to_internal_value(data)

    @staticmethod
    def cleanup_cpf(cpf):
        if cpf:
            return cpf.replace('.', '') \
                .replace('-', '')
        else:
            return ''


class SimpleUserRegistrationSerializer(serializers.ModelSerializer):
    city = serializers.PrimaryKeyRelatedField(queryset=City.objects.all())
    state = serializers.PrimaryKeyRelatedField(queryset=Region.objects.all())

    class Meta:
        model = User
        fields = ('email', 'username', 'zipcode', 'name', 'is_active', 'origin',
                  'city', 'state')

    def create(self, validated_data):
        user = super(SimpleUserRegistrationSerializer, self)\
            .create(validated_data)
        user.set_unusable_password()
        user.save()
        request = self.context.get('request', None)
        if request:
            setup_user_email(request, user, [])
        return user


class SimpleUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'image', 'name', 'biography')


class SenderUserSerializer(serializers.ModelSerializer):
    city = serializers.SlugRelatedField(read_only=True, slug_field='name')
    state = serializers.SlugRelatedField(read_only=True, slug_field='name')

    class Meta:
        model = User
        fields = ('name', 'city', 'state', 'cpf')


class UserStatsSerializer(serializers.Serializer):
    state_name = serializers.CharField(max_length=255, required=False)
    count = serializers.IntegerField()

    class Meta:
        fields = ('state_name', 'count')


class RegistrationSerializer(RegisterSerializer):

    name = serializers.CharField(max_length=255, required=False)

    captcha = serializers.CharField(max_length=2048)
    city = serializers.PrimaryKeyRelatedField(queryset=City.objects.all(),
                                              allow_null=True)
    state = serializers.PrimaryKeyRelatedField(queryset=Region.objects.all(),
                                               allow_null=True)
    phone_number = serializers.CharField(max_length=255, allow_null=True)
    origin = serializers.CharField(max_length=30, required=False, allow_blank=True)

    def get_cleaned_data(self):
        return {
            'name': self.validated_data.get('name', ''),
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'city': self.validated_data.get('city', None),
            'state': self.validated_data.get('state', None),
            'phone_number': self.validated_data.get('phone_number', ''),
            'origin': self.validated_data.get('origin', 'signup')
        }

    def validate_captcha(self, attr):
        captcha_req = requests.post('https://www.google.com/recaptcha/api/siteverify',
            data = {
                'secret': settings.RECAPTCHA_SECRET_KEY,
                'response': attr
            })
        captcha_resp = captcha_req.json()
        if not captcha_resp['success']:
            raise serializers.ValidationError(_('Invalid CAPTCHA.'))

    def custom_signup(self, request, user):
        user.name = self.cleaned_data['name']
        user.phone_number = self.cleaned_data['phone_number']
        user.city = self.cleaned_data['city']
        user.state = self.cleaned_data['state']
        user.origin = self.cleaned_data['origin']
        user.save()

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user


class PoliticalFrontSerializer(serializers.ModelSerializer):

    class Meta:
        model = PoliticalFront
        fields = ('id', 'name')


class ActivismSchoolWorkplaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ActivismSchoolWorkplace
        fields = ('id', 'name')


class UjsManagerRoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = UjsManagerRole
        fields = ('id', 'name')


class FixSocialLoginSerializer(SocialLoginSerializer):
        def validate(self, attrs):
            view = self.context.get('view')
            request = self._get_request()

            if not view:
                raise serializers.ValidationError(
                    _("View is not defined, pass it as a context variable")
                )

            adapter_class = getattr(view, 'adapter_class', None)
            if not adapter_class:
                raise serializers.ValidationError(_("Define adapter_class in view"))

            adapter = adapter_class(request)
            app = adapter.get_provider().get_app(request)

            # More info on code vs access_token
            # http://stackoverflow.com/questions/8666316/facebook-oauth-2-0-code-and-token

            # Case 1: We received the access_token
            if attrs.get('access_token'):
                access_token = attrs.get('access_token')

            # Case 2: We received the authorization code
            elif attrs.get('code'):
                self.callback_url = getattr(view, 'callback_url', None)
                self.client_class = getattr(view, 'client_class', None)

                if not self.callback_url:
                    raise serializers.ValidationError(
                        _("Define callback_url in view")
                    )
                if not self.client_class:
                    raise serializers.ValidationError(
                        _("Define client_class in view")
                    )

                code = attrs.get('code')

                provider = adapter.get_provider()
                scope = provider.get_scope(request)
                client = self.client_class(
                    request,
                    app.client_id,
                    app.secret,
                    adapter.access_token_method,
                    adapter.access_token_url,
                    self.callback_url,
                    scope
                )
                token = client.get_access_token(code)
                access_token = token['access_token']

            else:
                raise serializers.ValidationError(
                    _("Incorrect input. access_token or code is required."))

            social_token = adapter.parse_token({'access_token': access_token})
            social_token.app = app

            try:
                login = self.get_social_login(adapter, app, social_token, access_token)
                complete_social_login(request, login)
            except HTTPError:
                raise serializers.ValidationError(_('Incorrect value'))

            if not login.is_existing:
                # We have an account already signed up in a different flow
                # with the same email address: raise an exception.
                # This needs to be handled in the frontend. We can not just
                # link up the accounts due to security constraints
                if(allauth_settings.UNIQUE_EMAIL):
                    if login.user.email and get_user_model().objects.filter(
                        email=login.user.email,
                    ).exists():
                        # There is an account already
                        user = get_user_model().objects.get(email=login.user.email)
                        if user.has_usable_password():
                            raise serializers.ValidationError(
                                          ("A user is already registered with this e-mail address."))
                        else:
                            login.user = user

                login.lookup()
                login.save(request, connect=True)
            attrs['user'] = login.account.user

            return attrs
