from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'ocupa.users'
    verbose_name = "Users"
