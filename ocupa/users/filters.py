from cities_light.models import Region
from django.contrib.auth import get_user_model
from django.db.models import Count, F, QuerySet, Value, IntegerField
from rest_framework import filters

User = get_user_model()

class UserStatsFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        assert not('has_login' in request.query_params and
                   'exclude_from_import' in request.query_params) is True, (
            "has_login and exclude_from_import not allowed together"
        )

        if 'has_login' in request.query_params:
            queryset = queryset.filter(from_import=True) \
                    .exclude(last_login=None)

        if 'exclude_from_import' in request.query_params:
            queryset = queryset.exclude(from_import=True)

        if 'no_state' in request.query_params:
            queryset = queryset.filter(state=None)

        if 'per_state' in request.query_params:
            queryset = queryset \
                .exclude(state=None) \
                .annotate(state_name=F('state__name'))\
                .values('state_name')\
                .annotate(count=Count('id')).order_by('state_name')
        else:
            queryset = {'state_name': 'all', 'count' : queryset.count()}

        if isinstance(queryset, QuerySet):
            regions = Region.objects\
                .values(state_name=F('name'))\
                .annotate(count=Value(0, IntegerField()))

            for r in regions:
                if queryset.filter(state_name=r['state_name']).exists():
                    r['count'] = queryset \
                        .get(state_name=r['state_name'])['count']

            queryset = list(regions)

        return queryset
