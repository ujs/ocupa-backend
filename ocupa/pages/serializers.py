from django.contrib.flatpages.models import FlatPage
from rest_framework import serializers


class FlatpageSerializer(serializers.ModelSerializer):

    class Meta:
        model = FlatPage
        fields = ['id', 'url', 'title', 'content',]
