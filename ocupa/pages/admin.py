from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin

class PageForm(FlatpageForm):

    class Meta:
        model = FlatPage
        fields = '__all__'
        widgets = {
            'content': CKEditorWidget()
        }

class PageAdmin(FlatPageAdmin):
    """
    Page Admin
    """
    form = PageForm

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, PageAdmin)
