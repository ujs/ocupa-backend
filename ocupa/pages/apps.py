from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'ocupa.pages'
    verbose_name = "Pages"
