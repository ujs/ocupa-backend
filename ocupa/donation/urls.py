from rest_framework.routers import DefaultRouter

from ocupa.donation import views

router = DefaultRouter()
router.register(
    r'',
    views.PagSeguroViewSet,
    base_name='pagseguro'
),
router.register(
    r'pre-approvals',
    views.PreApprovalViewSet,
    base_name='pre-approvals'
)
router.register(
    r'campaigns',
    views.CampaignViewSet,
    base_name='campaign'
)

urlpatterns = [
]

urlpatterns.extend(router.urls)
