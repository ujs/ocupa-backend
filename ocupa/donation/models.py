# coding=utf-8
from autoslug import AutoSlugField
from django.contrib.postgres.fields import JSONField, ArrayField
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from cities_light.models import City, Region, Country

PAYMENT_METHODS = (
    ('creditCard', _('Credit card')),
    ('boleto', _('Boleto')),
    ('eft', _('Debit card')),
)

PAYMENT_PERIODS = (
    ('single', _('Single')),
    ('weekly', _('Weekly')),
    ('monthly', _('Monthly'))
)

PAYMENT_TYPES = (
    ('donation', _('Donation')),
    ('registration', _('Event Registration')),
    ('subscription', _('Subscription Payment'))
)

BOOLEAN_CHOICES = (
    (False, _('No')),
    (True, _('Yes')),
)


class BillingAddress(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )
    postal_code = models.CharField(_("Postal code"), max_length=9)
    street = models.CharField(_("Street address"), max_length=255)
    number = models.CharField(_("Address number"), max_length=10)
    complement = models.CharField(_("Address complement"), max_length=255, blank=True)
    district = models.CharField(_("District"), max_length=255)
    city = models.ForeignKey(City, related_name='billing_addresses', null=True, blank=True)
    state = models.ForeignKey(Region, related_name='billing_addresses', null=True, blank=True)
    country = models.ForeignKey(Country, related_name='billing_addresses', null=True, blank=True)

    def __str__(self):
        return self.user.name


    class Meta:
        verbose_name = _('Billing Address')
        verbose_name_plural = _('Billing Addresses')


class Campaign(models.Model):
    name = models.CharField(
        _('Campaign name'),
        max_length=255,
    )
    reference = models.CharField(
        _('Reference'),
        max_length=50,
    )
    description = models.TextField(
        _('Description'),
        null=True,
        blank=True
    )
    goal = models.FloatField(
        _('Goal'),
        default=0.0
    )
    raised = models.FloatField(
        _('Raised'),
        null=True,
        blank=True
    )
    donations_count = models.FloatField(
        _('Donations Count'),
        null=True,
        blank=True
    )
    slug = AutoSlugField(
        unique=True,
        populate_from='name',
        slugify=slugify
    )
    payment_methods = ArrayField(
        models.CharField(max_length=50,),
        verbose_name=_('Payment Methods'),
        default=list,
        null=True,
        blank=True
    )
    payment_types = ArrayField(
        models.CharField(max_length=50,),
        verbose_name=_('Payment Types'),
        default=list,
        null=True,
        blank=True
    )
    suggested_values = ArrayField(
        models.FloatField(_('Value')),
        verbose_name=_('Suggested Values'),
        default=list,
        null=True,
        blank=True
    )
    rubric = ArrayField(
        models.CharField(max_length=50,),
        verbose_name=_('Rubric'),
        default=list,
        null=True,
        blank=True
    )
    is_registration_process = models.BooleanField(default=False)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _('Campaign')
        verbose_name_plural = _('Campaigns')

    @property
    def suggested_values_ordered(self):
        return sorted(self.suggested_values)


class PreApprovalPlan(models.Model):
    CHARGE_TYPE = (
        ('AUTO', 'Automática'),
        ('MANUAL', 'Manual')
    )
    reference = models.CharField(_('Reference'), max_length=255)
    name = models.CharField(_('Plan Name'), max_length=255)
    details = models.CharField(_('Details'), max_length=255)
    charge = models.CharField(
        _('Charge'),
        max_length=30,
        choices=CHARGE_TYPE,
        default=CHARGE_TYPE[0][0]
    )
    period = models.CharField(_('Period'), max_length=255)
    amount_per_payment = models.FloatField(
        _('Amount'),
        null=True,
        blank=True
    )
    expiration = JSONField(_('Expiration'), blank=True, null=True)
    max_uses = models.IntegerField(_('Max Uses'), default=1)
    slug = AutoSlugField(
        unique=True,
        populate_from='name'
    )
    code = models.CharField(_('Code'), max_length=255, null=True)

    campaign = models.ForeignKey(
        Campaign,
        null=True,
        verbose_name=_('Campaign')
    )

    ## TODO: FIELDS TO EXPLORE LATER
    # pa_membership_fee = models.FloatField(_('Fee'), null=True, blank=True)
    # pa_trial_period_duration = models.IntegerField(_(''), null=True, blank=True)
    # pa_expiration_unit = models.CharField(_(''), max_length=255)
    # pa_maxAmountPerPeriod = models.FloatField(_(''), null=True, blank=True)
    # pa_maxAmountPerPayment = models.FloatField(_(''), null=True, blank=True)
    # pa_max_total_amount = models.FloatField(_(''), null=True, blank=True)
    # pa_maxPaymentsPerPeriod = models.IntegerField(_(''), null=True, blank=True)
    # pa_initialDate = models.DateTimeField(_(''))
    # pa_finalDate = models.DateTimeField(_(''))
    # pa_dayOfYear = models.CharField(_(''), max_length=255)
    # pa_dayOfMonth = models.IntegerField(_(''), null=True, blank=True)
    # pa_dayOfWeek = models.CharField(_(''), max_length=255)
    # pa_cancelURL = models.CharField(_(''), max_length=255)
    # reviewURL = models.CharField(_(''), max_length=255)
    # redirectURL = models.CharField(_(''), max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Pre Approval Plan')
        verbose_name_plural = _('Pre Approval Plans')


class PreApprovalSubscription(models.Model):
    pre_approval_plan = models.ForeignKey(
        PreApprovalPlan,
        related_name=_('subscriptions')
    )
    code = models.CharField(_('Code'), max_length=255, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        on_delete=models.CASCADE,
        related_name='subscriptions'
    )
    rubric = models.CharField(
        _('Rubric'),
        max_length=50,
        null=True,
        blank=True
    )
    cancelation_token = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    token_expiration_date = models.DateTimeField(
        blank=True,
        null=True
    )
    cancelation_date = models.DateTimeField(
        null=True,
        blank=True
    )
    value = models.FloatField(
        _('Value'),
        null=True,
        blank=True
    )
    start_date = models.DateTimeField(
        null=True,
        blank=True
    )

    def __str__(self):
        return '{}, plano {}'.format(self.user.name, self.pre_approval_plan.name,)

    class Meta:
        verbose_name = _('Subscription')
        verbose_name_plural = _('Subscriptions')


class PagSeguroCheckout(models.Model):
    STATUS = (
        (1, u'Aguardando Pagamento'),
        (2, u'Em análise'),
        (3, u'Paga'),
        (4, u'Disponível'),
        (5, u'Em disputa'),
        (6, u'Devolvida'),
        (7, u'Cancelada'),
        (8, u'Inválida')
    )
    session = models.CharField(
        _('Session'),
        max_length=255,
        blank=True,
        null=True,
    )
    transaction = models.CharField(
        _('Transaction ID'),
        max_length=255,
        unique=True,
        blank=True
    )
    payment_method = models.CharField(
        _('Payment method'),
        null=True,
        choices=PAYMENT_METHODS,
        max_length=255,
        blank=True
    )
    sender = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name='transactions',
        on_delete=models.CASCADE
    )
    status = models.SmallIntegerField(
        _('Payment status'),
        null=True,
        choices=STATUS,
        blank=True
    )
    value = models.FloatField(_('Donation amount'), blank=True)
    campaign = models.ForeignKey(
        Campaign,
        blank=True,
        null=True,
        related_name='transactions',
        on_delete=models.CASCADE
    )
    extra_info = JSONField(_('info'), blank=True, null=True)
    created_at = models.DateTimeField(
        _('Created at'),
        auto_now_add=True,
        null=True
    )
    updated_at = models.DateTimeField(_('Updated at'), auto_now=True)
    rubric = models.CharField(
        _('Rubric'),
        max_length=50,
        null=True,
        blank=True
    )
    type = models.CharField(
        _('Transaction Type'),
        max_length=50,
        choices=PAYMENT_TYPES,
        default=PAYMENT_TYPES[0][0]
    )
    subscription = models.ForeignKey(
        PreApprovalSubscription,
        null=True,
        related_name='transactions'
    )
    paid_out = models.BooleanField(
        _('Paid out'),
        choices=BOOLEAN_CHOICES,
        default=False)

    class Meta:
        verbose_name = _('Donation and Subscription')
        verbose_name_plural = _('Donations and Subscriptions')
