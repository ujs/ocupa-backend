from collections import OrderedDict

from rest_framework import serializers
from rest_framework.fields import SkipField
from rest_framework.relations import PKOnlyObject

from ocupa.donation.models import Campaign, PagSeguroCheckout, \
    BillingAddress, PreApprovalPlan
from ocupa.users.serializers import SenderUserSerializer

class CampaignSerializer(serializers.ModelSerializer):
    suggested_values = serializers.ListField(source='suggested_values_ordered')

    class Meta:
        model = Campaign
        fields = '__all__'


class BillingAddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = BillingAddress
        fields = '__all__'


class PagSeguroCheckoutSerializer(serializers.ModelSerializer):
    sender = SenderUserSerializer(read_only=True,)

    class Meta:
        model = PagSeguroCheckout
        fields = '__all__'


class PreApprovalPlanSerializer(serializers.ModelSerializer):
    campaign = serializers.SlugRelatedField(slug_field='slug',
                                            queryset=Campaign.objects.all())
    class Meta:
        model = PreApprovalPlan
        fields = '__all__'


class PagSeguroBaseSerializer(serializers.Serializer):
    def to_representation(self, instance):
        if not (getattr(self, 'Meta', None) and
                getattr(self.Meta, 'aliases', None)):
            return super(PagSeguroBaseSerializer, self)\
                .to_representation(instance)

        ret = OrderedDict()
        fields = self._readable_fields

        for field in fields:
            try:
                attribute = field.get_attribute(instance)
            except SkipField:
                continue

            check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
            if check_for_none is None:
                ret[field.field_name] = None
            elif field.field_name in self.Meta.aliases:
                ret[self.Meta.aliases[field.field_name]] = \
                    field.to_representation(attribute)
            else:
                ret[field.field_name] = field.to_representation(attribute)

        return ret


class PagSeguroTransactionSerializer(PagSeguroBaseSerializer):
    currency = serializers.CharField(default='BRL')
    payment_mode = serializers.CharField(default='default')
    payment_method = serializers.CharField(required=True)
    receiver_email = serializers.EmailField(required=True)
    shipping_address_required = serializers.BooleanField(default=False)
    reference = serializers.CharField(required=True)
    credit_card_token = serializers.CharField(required=True)
    installment_quantity = serializers.CharField(required=True)
    installment_value = serializers.CharField(required=True)


class PhoneSerializer(PagSeguroBaseSerializer):
    area_code = serializers.CharField(max_length=2, required=True)
    number = serializers.CharField(max_length=9, required=True)

    class Meta:
        aliases = {
            'area_code': 'areaCode',
            'number': 'number',
        }


class DocumentSerializer(PagSeguroBaseSerializer):
    type = serializers.CharField(max_length=5)
    value = serializers.CharField(max_length=11, required=True)


class SenderSerializer(PagSeguroBaseSerializer):
    name = serializers.CharField(required=True)
    cpf = serializers.CharField(max_length=11, required=True)
    email = serializers.EmailField(required=True)
    area_code = serializers.CharField(max_length=2, required=True)
    phone = serializers.CharField(max_length=9, required=True)
    hash = serializers.CharField(required=True)

    class Meta:
        aliases = {
            'name': 'senderName',
            'cpf': 'senderCPF',
            'email': 'senderEmail',
            'area_code': 'senderAreaCode',
            'phone': 'senderPhone',
            'hash': 'senderHash',
        }


class PreApprovalSenderSerializer(PagSeguroBaseSerializer):
    name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    hash = serializers.CharField(required=True)
    phone = PhoneSerializer()
    documents = DocumentSerializer()


class CreditCardSerializer(PagSeguroBaseSerializer):
    name = serializers.CharField(required=True)
    cpf = serializers.CharField(max_length=11, required=True)
    area_code = serializers.CharField(max_length=2, required=True)
    phone = serializers.CharField(max_length=9, required=True)
    birth_date = serializers.DateField(required=True, format='%d/%m/%Y',
                                              input_formats=['%d/%m/%Y', 'iso-8601'])

    class Meta:
        aliases = {
            'name': 'creditCardHolderName',
            'cpf': 'creditCardHolderCPF',
            'area_code': 'creditCardHolderAreaCode',
            'phone': 'creditCardHolderPhone',
            'birth_date': 'creditCardHolderBirthDate'
        }


class PreApprovalHolderSerializer(PagSeguroBaseSerializer):
    name = serializers.CharField(required=True)
    birth_date = serializers.DateField(required=True, format='%d/%m/%Y',
                                       input_formats=['%d/%m/%Y', 'iso-8601'])
    phone = PhoneSerializer()
    documents = DocumentSerializer()

    class Meta:
        aliases = {
            'birth_date': 'birthDate',
        }


class AddressSerializer(PagSeguroBaseSerializer):
    street = serializers.CharField(required=True)
    number = serializers.CharField(required=True)
    complement = serializers.SerializerMethodField(required=False)
    district = serializers.CharField(required=True)
    postal_code = serializers.CharField(required=True)
    city = serializers.CharField(required=True)
    state_uf = serializers.CharField(required=True)
    country = serializers.CharField(required=True)

    def get_complement(self, data):
        if 'complement' in data:
             return data['complement']
        else:
            return ''


class PagSeguroBillingAddressSerializer(AddressSerializer):
    class Meta:
        aliases = {
            'installment_quantity': 'installmentQuantity',
            'installment_value': 'installmentValue',
            'street': 'billingAddressStreet',
            'number': 'billingAddressNumber',
            'complement': 'billingAddressComplement',
            'district': 'billingAddressDistrict',
            'postal_code': 'billingAddressPostalCode',
            'city': 'billingAddressCity',
            'state_uf': 'billingAddressState',
            'country': 'billingAddressCountry',
        }


class PreApprovalAddressSerializer(AddressSerializer):
    class Meta:
        aliases = {
            'postal_code': 'postalCode',
            'state_uf': 'state'
        }

