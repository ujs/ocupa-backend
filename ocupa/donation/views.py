# coding=utf-8
import hashlib
import json
import datetime
import random
from xml.etree.ElementTree import ParseError
from xml.etree import ElementTree as etree

import pytz
import requests
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from cities_light.models import City
from django.contrib.auth import get_user_model
from django.template import TemplateDoesNotExist
from django.utils.http import urlsafe_base64_encode as id_encoder, \
    urlsafe_base64_decode as id_decoder
from rest_framework.decorators import list_route, detail_route
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status, viewsets

from config.settings import base
from ocupa.users.serializers import UserSerializer
from .permissions import IsSenderOrAdmin
from .serializers import CampaignSerializer, CreditCardSerializer, \
    SenderSerializer, PagSeguroBillingAddressSerializer, \
    PagSeguroCheckoutSerializer, BillingAddressSerializer, \
    PreApprovalPlanSerializer, PreApprovalSenderSerializer, \
    PreApprovalHolderSerializer, PreApprovalAddressSerializer
from ocupa.donation.models import Campaign, PagSeguroCheckout, \
    PreApprovalPlan, PreApprovalSubscription, PAYMENT_METHODS, PAYMENT_TYPES

User = get_user_model()


class PagSeguroViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PagSeguroCheckoutSerializer
    queryset = PagSeguroCheckout.objects.all()
    lookup_field = 'transaction'
    permission_classes = (IsSenderOrAdmin,)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def session(self, request):
        auth_data = {
            "email": base.PAGSEGURO_EMAIL,
            "token": base.PAGSEGURO_TOKEN
        }
        try:
            req = requests.post(base.PAGSEGURO_URL + '/v2/sessions',
                              data=auth_data)
        except requests.ConnectionError:
            return Response({
                'error': True,
                'message': u'Erro de conexão com o PagSeguro, tente mais tarde',
            }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        try:
            session_id = etree.fromstring(req.text).find('id').text
        except ParseError:
            return Response({
                'error': True,
                'message': u'Dados recebidos não puderam ser processados',
                'raw_data': req.text,
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response({'session': session_id}, status=status.HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def transaction(self, request):
        sender = request.data['sender']
        payment_data = request.data['payment']

        sender['phone']['number'] = sender['phone']['number'].replace('-', '')
        sender_phone = sender['phone']
        user, sender_data = handle_sender(sender, payment_data, request)
        if isinstance(sender_data, Response):
            return sender_data
        city = City.objects.get(pk=payment_data['city'])
        payment_data.update({
            'city': city.name,
            'country': "BRA"
        })

        items, installment_value = get_items(request.data['items'])
        auth_data = {
            "email": base.PAGSEGURO_EMAIL,
            "token": base.PAGSEGURO_TOKEN
        }
        auth_data.update(items)

        transaction_data = {
            'currency': 'BRL',
            'paymentMode': 'default',
            'paymentMethod': request.data['method'],
            'receiverEmail': base.PAGSEGURO_EMAIL,
            'shippingAddressRequired': 'false',
            'reference': request.data['reference'],
            'installmentQuantity': 1,
            'installmentValue': '%.2f' % installment_value,
        }
        transaction_data.update(sender_data)
        if request.data['method'] == PAYMENT_METHODS[0][0]:
            cc_data = handle_holder(payment_data, sender_phone)
            if isinstance(cc_data, Response):
                return cc_data
            else:
                transaction_data.update({
                    'creditCardToken': request.data['card_token'],
                })
                transaction_data.update(cc_data)
        billing_data = handle_billing(payment_data)
        if isinstance(billing_data, Response):
            return billing_data
        else:
            transaction_data.update(billing_data)

        auth_data.update(transaction_data)
        headers = {
            'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
        }
        try:
            r = requests.post(base.PAGSEGURO_URL + '/v2/transactions/',
                              headers=headers, data=auth_data)
        except requests.ConnectionError:
            return Response({
                'error': True,
                'message': u'Erro de conexão com o PagSeguro, tente mais tarde',
            }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        try:
            errors = {}
            tree = etree.fromstring(r.text)
            for e in tree.iter('error'):
                errors[str(e.find('code').text)] = e.find('message').text

            result = {}
            if not errors:
                checkout = create_checkout(request.data, installment_value,
                                           billing_data, user)

                if isinstance(checkout, Response):
                    return checkout

                billing_address = create_billing_address(user, payment_data,
                                                         city)
                if isinstance(billing_address, Response):
                    return billing_address

                for e in tree.iter('transaction'):
                    result["code"] = e.find('code').text
                    result["date"] = e.find('date').text
                    if request.data['method'] == 'boleto':
                        result["paymentLink"] = e.find('paymentLink').text
                        checkout.extra_info['payment_link'] = \
                            result["paymentLink"]
                        checkout.save()
                checkout.transaction = result["code"]
                checkout.save()
        except ParseError:
            return Response({
                'error': True,
                'message': u'Dados recebidos não puderam ser processados',
                'result': {'raw_data': r.text},
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        result["raw_data"] = r.text

        if len(errors) == 0 and checkout.campaign:
            try:
                adapter = get_adapter(request)
                msg = adapter.render_mail(
                    'campaign/email/transaction',
                    user.email, {
                        "user": user,
                        "checkout": checkout,
                        "payment_link": result.get('paymentLink', None)
                    })
                msg.send()
            except TemplateDoesNotExist:
                pass

        return Response({
            'error': len(errors) > 0,
            'result': result,
            'errors': errors
        }, status=status.HTTP_200_OK)

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def notification(self, request):

        data = self.get_transaction_data(request.data['notificationCode'])
        if isinstance(data, Response):
            return data
        return Response({}, status=status.HTTP_200_OK)

    def get_transaction_data(self, code):
        headers = {
            'Content-type': 'application/x-www-form-urlencoded;charset=ISO-8859-1',
        }

        try:
            url = base.PAGSEGURO_URL + \
                  '/v3/transactions/notifications/{}/?email={}&token={}' \
                .format(code, base.PAGSEGURO_EMAIL, base.PAGSEGURO_TOKEN)
            r = requests.get(url, headers=headers)
        except requests.ConnectionError:
            return Response({
                'error': True,
                'errors': u'Erro de conexão com o PagSeguro, tente mais tarde',
            }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        try:
            errors = {}
            tree = etree.fromstring(r.text)
            for e in tree.iter('error'):
                errors[str(e.find('code').text)] = e.find('message').text

            checkout_data = {}
            if not errors:
                trans_status = int(tree.find('status').text)
                if PagSeguroCheckout.objects \
                    .filter(transaction=tree.find('code').text) \
                    .exists():
                    checkout = PagSeguroCheckout.objects \
                        .get(transaction=tree.find('code').text)
                    checkout.status = trans_status
                    checkout.save()
                    return checkout

                checkout_data['transaction'] = tree.find('code').text
                i = int(tree.find('paymentMethod').find('type').text) - 1
                checkout_data['payment_method'] = \
                    PAYMENT_METHODS[i][0]

                checkout_data['status'] = trans_status
                checkout_data['value'] = float(tree.find('grossAmount') \
                                               .text)
                plan_name = tree.find('items').find('item')\
                    .find('description').text
                value, slug, rubric = plan_name.split('@')

                checkout_data['value'] = value
                checkout_data['campaign'] = Campaign.objects.get(slug=slug).id
                checkout_data['rubric'] = rubric
                serializer = PagSeguroCheckoutSerializer(data=checkout_data)
                if serializer.is_valid():
                    email = tree.find('sender').find('email').text
                    try:
                        sender = User.objects.get(email=email)
                        subscription = PreApprovalSubscription.objects.get(
                            cancelation_date=None,
                            user__email=email,
                            pre_approval_plan__name=plan_name,
                            rubric=rubric
                        )
                    except User.DoesNotExist:
                        return Response({
                            'error': True,
                            'message': u'invalid user email',
                            'result': {'raw_data': r.text},
                        }, status=status.HTTP_400_BAD_REQUEST)
                    except PreApprovalSubscription.DoesNotExist:
                        subscription = None

                    checkout = serializer.save()
                    checkout.sender = sender
                    checkout.subscription = subscription
                    if int(tree.find('type').text) == 11:
                        checkout.type = PAYMENT_TYPES[2][0]
                    checkout.save()
                else:
                    return Response({
                        'error': True,
                        'message': serializer.errors,
                        'result': {'raw_data': r.text},
                    }, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({
                    'error': True,
                    'message': u'',
                    'result': {'raw_data': r.text},
                }, status=status.HTTP_400_BAD_REQUEST)
        except ParseError:
            return Response({
                'error': True,
                'message': u'Dados recebidos não puderam ser processados',
                'result': {'raw_data': r.text},
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PreApprovalViewSet(viewsets.GenericViewSet):
    @list_route(methods=['POST'], url_path='subscribe',
                permission_classes=[AllowAny])
    def pre_approvals(self, request):
        sender = request.data['sender']
        payment_data = request.data['payment']
        rubric = request.data.get('rubric', '')
        campaign_slug = request.data['campaign'],

        sender['phone']['number'] = sender['phone']['number'].replace('-', '')
        sender['phone']['area_code'] = sender['phone']['areaCode']
        sender['documents'] = {
            'type': 'CPF',
            'value': get_cpf(sender['cpf'])
        }
        headers = {
            'Content-type': 'application/json;charset=ISO-8859-1',
            'Accept': 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
        }

        items, installment_value = get_items(request.data['items'])
        user, _sender = handle_sender(sender, payment_data, request,
                                          PreApprovalSenderSerializer)

        if isinstance(_sender, Response):
            return _sender

        if PreApprovalSubscription.objects \
            .filter(cancelation_date=None,
                    user=user,
                    pre_approval_plan__campaign__slug=campaign_slug,
                    rubric=rubric) \
            .exists():
            subscription = PreApprovalSubscription.objects \
                .get(cancelation_date=None,
                     user=user,
                     pre_approval_plan__campaign__slug=campaign_slug,
                     rubric=rubric)
            pg_subscription = get_subscription(headers, subscription.code)
            if isinstance(pg_subscription, Response):
                return pg_subscription
            elif pg_subscription['status'] == 'ACTIVE':
                return Response({
                    'error': True,
                    'errors': 'Não foi possível realizar inscrição porque  \
                               o usuário já está inscrito nessa campanha \
                               para esse destino.'
                }, status=status.HTTP_200_OK)
            else:
                subscription.cancelation_date = datetime.datetime.now()
                subscription.save()

        plan_name = '{}@{}@{}'.format(installment_value,
                                      request.data['campaign'], rubric)
        if PreApprovalPlan.objects.filter(name=plan_name).exists():
            plan = PreApprovalPlan.objects.get(name=plan_name)
        else:
            data = {
                'reference': request.data['campaign'],
                'name': plan_name,
                'details': 'Plano da campanha {}, com valor {} e rubrica: {}'
                    .format(request.data['campaign'],
                            installment_value,
                            rubric),
                'charge': 'AUTO',
                'period': request.data['payment_type'],
                'amount_per_payment': installment_value,
                'expiration': {
                    'value': 1,
                    'unit': 'YEARS',
                },
                'max_uses': 1000000,
                'campaign': request.data['campaign']
            }
            plan = create_plan(headers, data)
            if isinstance(plan, Response):
                return plan

        _sender['documents'] = [_sender['documents']]
        sender_data = {
            'sender': _sender
        }

        city = City.objects.get(pk=payment_data['city'])
        payment_data.update({
            'city': city.name,
            'country': "BRA",
            'documents': {
                'type': 'CPF',
                'value': get_cpf(payment_data['holder_cpf'])
            }
        })
        cc_data = handle_holder(payment_data, sender['phone'],
                                     PreApprovalHolderSerializer)
        if isinstance(cc_data, Response):
            return cc_data
        else:
            cc_data['documents'] = [cc_data['documents']]
            sender_data.update({
                'paymentMethod': {
                    'type': PAYMENT_METHODS[0][0],
                    'creditCard': {
                        'token': request.data['card_token'],
                        'holder': cc_data
                    }
                }
            })

        billing_data = handle_billing(payment_data,
                                           PreApprovalAddressSerializer)
        if isinstance(billing_data, Response):
            return billing_data
        else:
            sender_data['paymentMethod']['creditCard']['holder'] \
                .update({
                'billingAddress': billing_data
            })
            sender_data['sender']['address'] = billing_data

        sender_data.update({
            'plan' : plan.code,
            'reference': plan.reference
        })

        result = join_plan(headers, sender_data)
        if isinstance(result, Response):
            return result

        current_date = datetime.datetime.now(tz=pytz.utc)
        subscription = PreApprovalSubscription(
            user=user,
            pre_approval_plan=plan,
            code=result['code'],
            rubric=rubric,
            value=installment_value,
            start_date=current_date
        )
        subscription.save()
        result = get_subscription(headers, subscription.code)
        if isinstance(result, Response):
            return result

        try:
            adapter = get_adapter(request)
            msg = adapter.render_mail('campaign/email/subscription'
                    .format(subscription.pre_approval_plan.campaign.slug.lower()),
                subscription.user.email, {
                    'user': subscription.user,
                    'campaign': subscription.pre_approval_plan.campaign,
                    'subscription': subscription,
                    'plan': subscription.pre_approval_plan,
                    'date': result['date'],
                    'request': request
                })
            msg.send()
        except TemplateDoesNotExist:
            pass

        return Response({
            'error': False,
            'result': result,
            'errors': {}
        }, status=status.HTTP_200_OK)

    @list_route(methods=['POST'], url_path='cancel',
                permission_classes=[AllowAny])
    def cancel(self, request):
        data = request.data
        email = data.get('email', None)
        campaign = data.get('campaign', None)
        rubric = data.get('rubric', '')
        if not (email and campaign):
            return Response({
                'error': True,
                'errors': 'Must send email, campaign and rubric'
            }, status=status.HTTP_400_BAD_REQUEST)
        try:
            subscription = PreApprovalSubscription.objects.get(
                cancelation_date=None,
                user__email=email,
                pre_approval_plan__campaign__slug=campaign['slug'],
                rubric=rubric
            )
        except PreApprovalSubscription.DoesNotExist:
            return Response({
                'error': True,
                'errors': 'Subscription not found'
            }, status=status.HTTP_404_NOT_FOUND)

        random.seed(datetime.datetime.now())
        seed = str(random.randint(0,10000000)) + subscription.code
        subscription.cancelation_token = generate_token(seed)

        now = datetime.datetime.now()
        subscription.token_expiration_date = now + datetime.timedelta(days=7)
        subscription.save()

        try:
            adapter = get_adapter(request)
            msg = adapter.render_mail('campaign/email/cancel_subscription'
                    .format(subscription.pre_approval_plan.campaign.slug.lower()),
                subscription.user.email, {
                    'user': subscription.user,
                    'campaign': subscription.pre_approval_plan.campaign,
                    'subscription': subscription,
                    'plan': subscription.pre_approval_plan,
                    'request': request
                })
            msg.send()
        except TemplateDoesNotExist:
            pass

        return Response({
            'error': False,
            'result': {'expiration': subscription.token_expiration_date}
        }, status=status.HTTP_200_OK)

    @list_route(methods=['GET'],
                url_path='cancel/(?P<code>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z_\-]+)',
                permission_classes=[AllowAny])
    def cancel_subscription_confirmation(self, request, code, token):
        try:
            subscription = PreApprovalSubscription.objects.get(code=code)
        except PreApprovalSubscription.DoesNotExist:
            return Response({
                'error': True,
                'errors': ['Subscription not found']
            }, status=status.HTTP_404_NOT_FOUND)

        current_date = datetime.datetime.now(tz=pytz.utc)
        if not subscription.cancelation_token or \
            not subscription.token_expiration_date or \
            current_date > subscription.token_expiration_date or \
            not(subscription.cancelation_token == token):
            return Response({
                'error': True,
                'errors': ['Token is invalid or expired']
            }, status=status.HTTP_400_BAD_REQUEST)

        response = self.cancel_subscription(code)

        if response.status_code == status.HTTP_200_OK:
            subscription.token_expiration_date = None
            subscription.cancelation_date = current_date
            subscription.save()

        return response

    def cancel_subscription(self, code):
        headers = {
            'Content-type': 'application/json;charset=ISO-8859-1',
            'Accept': 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
        }

        try:
            url = base.PAGSEGURO_URL + '/pre-approvals/{}/cancel/?email={}&token={}' \
                .format(code, base.PAGSEGURO_EMAIL, base.PAGSEGURO_TOKEN)
            r = requests.put(url, headers=headers)
            try:
                result = json.loads(r.content)
                if 'error' in result:
                    return Response({
                        'error': True,
                        'errors': result['errors'],
                    }, status=status.HTTP_400_BAD_REQUEST)
            except json.decoder.JSONDecodeError:
                if r.content:
                    return Response({
                        'error': True,
                        'errors': {'json_decode': r.content}
                    }, status=status.HTTP_400_BAD_REQUEST)
        except requests.ConnectionError:
            return Response({
                'error': True,
                'errors': u'Erro de conexão com o PagSeguro, tente mais tarde',
            }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        return Response({
            'error': False,
            'result': {
                'subscription': code,
                'status': 'Cancelada'
            }
        }, status=status.HTTP_200_OK)


class CampaignViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CampaignSerializer
    queryset = Campaign.objects.all()
    lookup_field = 'slug'


def get_cpf(cpf):
    if cpf:
        return cpf.replace('.', '') \
            .replace('-', '')
    else:
        return ''

def get_valid_username(username, user_id = None):
    i = 1
    while User.objects.filter(username=username).exists() and \
        not(User.objects.get(username=username).id == user_id):
        _email = username.split('@')
        _email[0] = _email[0] + '+{}'.format(str(i))
        username = '@'.join(_email)

    return username

def get_user_data(sender, address):
    sender['cpf'] = get_cpf(sender['cpf'])
    d = sender['birth_date'].split('/')
    sender['birth_date'] = '{}-{}-{}'.format(d[2], d[1], d[0])

    sender['address'] = address['street']
    sender['address_number'] = address['number']
    sender['address_extra'] = address.get('complement', '')
    sender['neighborhood'] = address['district']
    sender['zipcode'] = address['postal_code']
    sender['city'] = address['city']
    sender['state'] = address['state']

    return sender

def create_user(sender, address, request):
    sender = get_user_data(sender, address)
    sender['origin'] = 'donation'
    sender['username'] = get_valid_username(sender['email'])
    serializer = UserSerializer(data=sender)
    if serializer.is_valid():
        user = serializer.save()
    else:
        return True, serializer.errors

    setup_user_email(request, user, [])
    return False, user

def update_user(sender, instance, address):
    sender = get_user_data(sender, address)
    sender['username'] = get_valid_username(sender['email'], instance.id)
    serializer = UserSerializer(instance, data=sender)
    if serializer.is_valid():
        return False, serializer.save()
    else:
        return True, serializer.errors

def get_items(item_list):
    items = {}
    installmentValue = 0

    for i in range(0, len(item_list)):
        lbl = str(i + 1)
        item = item_list[i]
        items["itemId" + lbl] = item['id']
        items["itemDescription" + lbl] = item['description']
        items["itemQuantity" + lbl] = item['quantity']
        items["itemAmount" + lbl] = '%.2f' % item['amount']
        installmentValue += item['amount']

    return items, installmentValue

def create_checkout(data, installment_value, billing_data, user):
    checkout = PagSeguroCheckoutSerializer(data={
        'session': data['session_id'],
        'payment_method': data['method'],
        'status': 1,
        'value': installment_value,
        'extra_info': {},
        'rubric': data.get('rubric', '')
    })
    if checkout.is_valid():
        checkout = checkout.save()
        checkout.sender = user
        checkout.extra_info['billing_address'] = billing_data

        if data.get('campaign', None):
            campaign = Campaign.objects \
                .get(slug=data.get('campaign'))
            checkout.campaign = campaign
            if campaign.is_registration_process:
                checkout.type = PAYMENT_TYPES[1][0]
        checkout.save()
    else:
        return Response({
            'error': True,
            'message': checkout.errors,
        }, status=status.HTTP_400_BAD_REQUEST)

    return checkout

def create_billing_address(user, payment_data, city):
    payment_data.update({
        'user': user.id,
        'city': city.id,
        'country': city.country.id
    })
    billing_address = BillingAddressSerializer(data=payment_data)
    if billing_address.is_valid():
        billing_address.save()
    else:
        return Response({
            'error': True,
            'message': billing_address.errors,
        }, status=status.HTTP_400_BAD_REQUEST)

    return billing_address

def join_plan(headers, sender_data):
    try:
        url = base.PAGSEGURO_URL \
              + '/pre-approvals/?email={}&token={}' \
                  .format(base.PAGSEGURO_EMAIL, base.PAGSEGURO_TOKEN)
        r = requests.post(url, data=json.dumps(sender_data), headers=headers)
        try:
            result = json.loads(r.content)
        except json.decoder.JSONDecodeError:
            return Response({
                'error': True,
                'errors': {'json_decode': r.content}
            }, status=status.HTTP_400_BAD_REQUEST)

        if 'error' in result:
            return Response({
                'error': True,
                'errors': result['errors'],
            }, status=status.HTTP_400_BAD_REQUEST)
    except requests.ConnectionError:
        return Response({
            'error': True,
            'errors': u'Erro de conexão com o PagSeguro, tente mais tarde',
        }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

    return result

def create_plan(headers, data):
    plan_serializer = PreApprovalPlanSerializer(data=data)
    if plan_serializer.is_valid():
        plan = plan_serializer.save()
    else:
        return Response(
            plan_serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    plan_data = {
        "reference": plan.reference,
        "maxUses": plan.max_uses,
        "preApproval": {
            "name": plan.name,
            "details": plan.details,
            "charge": plan.charge,
            "period": plan.period,
            "amountPerPayment": str('%.2f'%plan.amount_per_payment),
            "expiration": plan.expiration
        }
    }

    try:
        url = base.PAGSEGURO_URL \
              + '/pre-approvals/request/?email={}&token={}' \
                  .format(base.PAGSEGURO_EMAIL, base.PAGSEGURO_TOKEN)
        r = requests.post(url, data=json.dumps(plan_data), headers=headers)
        result = json.loads(r.content)
        if 'error' in result:
            return Response({
                'error': True,
                'errors': result['errors'],
            }, status=status.HTTP_400_BAD_REQUEST)
        plan.code = json.loads(r.content)['code']
        plan.save()
    except requests.ConnectionError:
        return Response({
            'error': True,
            'errors': u'Erro de conexão com o PagSeguro, tente mais tarde',
        }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

    return plan

def handle_sender(sender, payment_data, request, serializer=None):
    sender_cpf = get_cpf(sender['cpf'])
    if not serializer:
        serializer = SenderSerializer
    if not User.objects.filter(email=sender['email']).exists():
        has_errors, user_data = create_user(sender, payment_data, request)
    else:
        has_errors, user_data = update_user(sender,
                                            User.objects.get(email=sender['email']),
                                            payment_data)

    if not has_errors:
        user = user_data
    else:
        return False, Response({
            'error': True,
            'message': user_data,
        }, status=status.HTTP_400_BAD_REQUEST)

    sender.update({
        'hash': request.data['sender_hash'],
    })
    if serializer == SenderSerializer:
        sender.update({
            'cpf': get_cpf(sender['cpf']),
            'area_code': sender['phone']['areaCode'],
            'phone': sender['phone']['number'],
        })

    sender_data = serializer(data=sender)
    if sender_data.is_valid():
        return user, sender_data.data
    else:
        return False, Response({
            'error': True,
            'message': sender_data.errors,
        }, status=status.HTTP_400_BAD_REQUEST)

def handle_holder(payment_data, sender_phone, serializer=None):
    if not serializer:
        serializer = CreditCardSerializer
    holder_cpf = get_cpf(payment_data['holder_cpf'])
    data = {
        'name': payment_data['holder_name'],
        'birth_date': payment_data['holder_birth_date'],
    }
    if serializer == CreditCardSerializer:
        data.update({
            'cpf': holder_cpf,
            'area_code': sender_phone['areaCode'],
            'phone': sender_phone['number'],
        })

    if serializer == PreApprovalHolderSerializer:
        data.update({
            'documents': payment_data['documents'],
            'phone': sender_phone
        })
    credit_card = serializer(data=data)
    if credit_card.is_valid():
        return credit_card.data
    else:
        return Response({
            'error': True,
            'errors': credit_card.errors,
        }, status=status.HTTP_400_BAD_REQUEST)

def handle_billing(payment_data, serializer=None):
    if not serializer:
        serializer = PagSeguroBillingAddressSerializer
    billing_data = serializer(data=payment_data)
    if billing_data.is_valid():
        return billing_data.data
    else:
        return Response({
            'error': True,
            'errors': billing_data.errors,
        }, status=status.HTTP_400_BAD_REQUEST)

def get_subscription(headers, code):
    try:
        url = base.PAGSEGURO_URL + '/pre-approvals/{}/?email={}&token={}' \
            .format(code, base.PAGSEGURO_EMAIL, base.PAGSEGURO_TOKEN)
        r = requests.get(url, headers=headers)
        result = json.loads(r.content)
        if 'error' in result:
            return Response({
                'error': True,
                'errors': result['errors'],
            }, status=status.HTTP_400_BAD_REQUEST)
    except requests.ConnectionError:
        return Response({
            'error': True,
            'errors': u'Erro de conexão com o PagSeguro, tente mais tarde',
        }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

    return result

def generate_token(seed):
    h256 = hashlib.sha256(seed.encode('utf-8'))
    digest = h256.hexdigest().encode()
    return id_encoder(digest)[0:20].decode()
