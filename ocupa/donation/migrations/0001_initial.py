# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-15 18:36
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cities_light', '0008_city_timezone'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BillingAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('postal_code', models.CharField(max_length=9, verbose_name='Postal code')),
                ('street', models.CharField(max_length=255, verbose_name='Street address')),
                ('number', models.IntegerField(verbose_name='Address number')),
                ('complement', models.CharField(max_length=255, verbose_name='Address complement')),
                ('district', models.CharField(max_length=255, verbose_name='District')),
                ('city', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='billing_addresses', to='cities_light.City')),
                ('country', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='billing_addresses', to='cities_light.Country')),
                ('state', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='billing_addresses', to='cities_light.Region')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PagSeguroCheckout',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('session', models.CharField(max_length=255, verbose_name='Session')),
                ('transaction', models.CharField(blank=True, max_length=255, verbose_name='Transaction ID')),
                ('payment_method', models.CharField(blank=True, choices=[('creditCard', 'Credit card'), ('eft', 'Debit card'), ('boleto', 'Boleto')], max_length=255, null=True, verbose_name='Payment method')),
                ('status', models.CharField(blank=True, choices=[(1, 'Aguardando Pagamento'), (2, 'Em análise'), (3, 'Paga'), (4, 'Disponível'), (5, 'Em disputa'), (6, 'Devolvida'), (7, 'Cancelada')], max_length=255, null=True, verbose_name='Payment status')),
                ('value', models.FloatField(blank=True, verbose_name='Donation amount')),
                ('sender', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='donations', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
