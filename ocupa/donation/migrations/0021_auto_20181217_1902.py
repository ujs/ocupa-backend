# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2018-12-17 21:02
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('donation', '0020_auto_20181215_0132'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagsegurocheckout',
            name='rubric',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Rubric'),
        ),
        migrations.AddField(
            model_name='preapprovalsubscription',
            name='value',
            field=models.FloatField(blank=True, null=True, verbose_name='Value'),
        ),
        migrations.AlterUniqueTogether(
            name='preapprovalsubscription',
            unique_together=set([('pre_approval_plan', 'user', 'rubric')]),
        ),
    ]
