# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-15 00:00
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('donation', '0004_auto_20180514_1517'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagsegurocheckout',
            name='extra_info',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='info'),
        ),
    ]
