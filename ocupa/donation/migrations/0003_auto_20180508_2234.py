# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-09 01:34
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('donation', '0002_auto_20180508_2028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaign',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from='name', unique=True),
        ),
    ]
