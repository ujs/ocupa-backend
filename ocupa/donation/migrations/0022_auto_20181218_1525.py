# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2018-12-18 17:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('donation', '0021_auto_20181217_1902'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='preapprovalsubscription',
            unique_together=set([]),
        ),
    ]
