import pytz
import requests
from datetime import timedelta, datetime
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from pip._vendor.html5lib.treebuilders import etree

from config.settings import base
from xml.etree.ElementTree import ParseError
from xml.etree import ElementTree as etree
from ocupa.donation.models import PagSeguroCheckout

User = get_user_model()

class Command(BaseCommand):
    help = 'Export users data in a csv file'

    def handle(self, *files, **options):
        queryset = PagSeguroCheckout.objects.filter(status__in=[1,2,5])

        for checkout in queryset:
            if (pytz.utc.localize(datetime.today()) -
                checkout.updated_at).total_seconds() > timedelta(hours=2).total_seconds():
                if checkout.transaction:
                    auth_data = {
                        "email": base.PAGSEGURO_EMAIL,
                        "token": base.PAGSEGURO_TOKEN,
                    }
                    try:
                        r = requests.get("{0}{1}{2}".format(base.PAGSEGURO_URL,
                                                             '/v2/transactions/',
                                                            checkout.transaction),
                                         params=auth_data)
                    except requests.ConnectionError:
                        pass

                    if not checkout.extra_info:
                        checkout.extra_info = {}

                    try:
                        if not (r.status_code == 404):
                            status = etree.fromstring(r.text).find('status').text
                            checkout.status = int(status)
                            if checkout.payment_method == 'boleto':
                                payment_link = etree.fromstring(r.text) \
                                    .find('paymentLink').text

                                checkout.extra_info['payment_link']  = payment_link
                        else:
                            checkout.status = 8
                    except ParseError:
                        pass
                    except AttributeError:
                        pass
                else:
                    checkout.status = 8
                checkout.save()
