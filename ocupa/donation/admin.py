import requests
from django import forms
from django.contrib import admin
from django.contrib.admin import RelatedOnlyFieldListFilter, \
    AllValuesFieldListFilter
from django.contrib.admin.filters import SimpleListFilter, \
    ChoicesFieldListFilter
from django.db import transaction
from django.db.models.aggregates import Sum
from django.utils.datastructures import MultiValueDict
from django.utils.translation import ugettext_lazy as _

from ocupa.donation.models import Campaign, PAYMENT_METHODS, PAYMENT_PERIODS
from ocupa.donation.models import PagSeguroCheckout, PreApprovalSubscription
from ckeditor.widgets import CKEditorWidget
from config.settings import base
from xml.etree.ElementTree import ParseError
from xml.etree import ElementTree as etree

class DropdownFilterTemplateMixin():
    template = "admin/dropdown_filter.html"


class RelatedOnlyDropdownListFilter(DropdownFilterTemplateMixin,
                                    RelatedOnlyFieldListFilter,):
    pass


class StringDropdownListFilter(DropdownFilterTemplateMixin,
                               AllValuesFieldListFilter):
    pass


class ChoicesDropdownListFilter(DropdownFilterTemplateMixin,
                                ChoicesFieldListFilter):
    pass


class StatusChoicesDropdownListFilter(ChoicesDropdownListFilter):
    def queryset(self, request, queryset):
        if self.lookup_val == None:
            return queryset\
                .exclude(status=2)\
                .exclude(status=5)\
                .exclude(status=6)\
                .exclude(status=7)\
                .exclude(status=8)
        return super().queryset(request, queryset)


class StatusFilter(DropdownFilterTemplateMixin, SimpleListFilter):
    title = _('Paid Out')

    parameter_name = 'paid_out'

    def lookups(self, request, model_admin):
        return (
            ('0', _('No')),
            ('1', _('Yes')),
            ('all', _('All'))
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        if self.value() in ('0', '1'):
            # v = int(self.value()) == 1
            queryset = queryset.filter(paid_out=self.value())
        elif self.value() == None:
            queryset = queryset.filter(paid_out=0)
        return queryset


@admin.register(PreApprovalSubscription)
class PreApprovalSubscriptionAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'get_status']
    list_display_links = None
    actions = None

    def get_status(self, obj):
        return _('Active') if obj.cancelation_date is None else _('Inactive')
    get_status.short_description = 'Status'


@admin.register(PagSeguroCheckout)
class PagSeguroCheckoutAdmin(admin.ModelAdmin):
    fields = ('status', 'value', 'campaign', 'extra_info', 'paid_out')
    search_fields = ['sender__name', 'sender__email']
    raw_id_fields = ['sender']
    list_display = ['get_name', 'sender', 'get_state', 'value', 'status', 'updated_at', 'paid_out']
    list_editable = ['paid_out']
    list_filter = [('campaign', RelatedOnlyDropdownListFilter),
                   ('rubric', StringDropdownListFilter), StatusFilter,
                   ('sender__state', RelatedOnlyDropdownListFilter),
                   ('status', StatusChoicesDropdownListFilter)]
    list_per_page = 100
    readonly_fields = ['status', 'value', 'campaign', ]
    date_hierarchy = 'updated_at'


    def get_name(self, obj):
        return obj.sender.name
    get_name.admin_order_field  = 'sender'  #Allows column order sorting
    get_name.short_description = _('Name')  #Renames column head

    def get_state(self, obj):
        return obj.sender.state
    get_state.admin_order_field = 'sender__state'
    get_state.short_description = _('State')


    actions = ['update_payment_status', 'set_paid']

    def update_payment_status(self, request, queryset):
        for checkout in queryset:
            if checkout.transaction:
                auth_data = {
                    "email": base.PAGSEGURO_EMAIL,
                    "token": base.PAGSEGURO_TOKEN,
                }
                try:
                    r = requests.get("{0}{1}{2}".format(base.PAGSEGURO_URL,
                                                         '/v2/transactions/',
                                                         checkout.transaction),
                                      params=auth_data)
                except requests.ConnectionError:
                    pass

                if not checkout.extra_info:
                    checkout.extra_info = {}

                try:
                    if not (r.status_code == 404):
                        status = etree.fromstring(r.text).find('status').text
                        checkout.status = int(status)
                        if checkout.payment_method == 'boleto':
                            payment_link = etree.fromstring(r.text) \
                                .find('paymentLink').text

                            checkout.extra_info['payment_link']  = payment_link
                    else:
                        checkout.status = 8
                except ParseError:
                    pass
                except AttributeError:
                    pass
            else:
                checkout.status = 8
            checkout.save()
    update_payment_status.short_description = _('Update Payment Status')

    def set_paid(self, request, queryset):
        with transaction.atomic():
            for c in queryset:
                c.paid_out = True
                c.save()
    set_paid.short_description = _('Set transactions paid out')

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context)
        if hasattr(response, 'context_data') and 'cl' in response.context_data:
            queryset = response.context_data['cl'].queryset
            extra_context = {
                'stats': {
                    'payments_total': self.get_payments_total(queryset),
                    'available': self.get_available_total(queryset),
                    'waiting_payment': self.get_waiting_total(queryset),
                    'paid_out': self.get_paid_out_total(queryset)
                }
            }
            response.context_data.update(extra_context)
        return response

    def get_available_total(self, queryset):
        result = queryset.filter(status=4).aggregate(total=Sum('value'))
        return result['total'] or 0

    def get_payments_total(self, queryset):
        result = queryset.filter(status=3).aggregate(total=Sum('value'))
        return result['total'] or 0

    def get_waiting_total(self, queryset):
        result = queryset.filter(status=1).aggregate(total=Sum('value'))
        return result['total'] or 0

    def get_paid_out_total(self, queryset):
        result = queryset\
            .filter(paid_out=False)\
            .filter(status=4)\
            .aggregate(total=Sum('value'))
        return result['total'] or 0

    class Media:
        css = {
             'all': ('css/filters.css',)
        }


class ArrayFieldSelectMultiple(forms.SelectMultiple):
    """This is a Form Widget for use with a Postgres ArrayField. It implements
    a multi-select interface that can be given a set of `choices`.

    You can provide a `delimiter` keyword argument to specify the delimeter used.

    """

    def __init__(self, *args, **kwargs):
        # Accept a `delimiter` argument, and grab it (defaulting to a comma)
        self.delimiter = kwargs.pop("delimiter", ",")
        super(ArrayFieldSelectMultiple, self).__init__(*args, **kwargs)

    # def render_options(self, choices, value):
    def format_value(self, value):
        # value *should* be a list, but it might be a delimited string.
        if isinstance(value, str):  # python 2 users may need to use basestring instead of str
            value = value.split(self.delimiter)
        # return super(ArrayFieldSelectMultiple, self).render_options(choices, value)
        return super(ArrayFieldSelectMultiple, self).format_value(value)

    def value_from_datadict(self, data, files, name):
        if isinstance(data, MultiValueDict):
            # Normally, we'd want a list here, which is what we get from the
            # SelectMultiple superclass, but the SimpleArrayField expects to
            # get a delimited string, so we're doing a little extra work.
            return self.delimiter.join(data.getlist(name))
        return data.get(name, None)


class CampaignAdminForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = '__all__'
        widgets = {
            'description': CKEditorWidget(),
            'payment_methods': ArrayFieldSelectMultiple(
                choices=PAYMENT_METHODS,
            ),
            'payment_types': ArrayFieldSelectMultiple(
                choices=PAYMENT_PERIODS,
            )
        }


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display = ['name', 'reference', 'slug', 'goal', 'raised']
    form = CampaignAdminForm
