# Ocupa

Projeto Ocupa baseado no padrão [cookiecutter-django](http://cookiecutter-django.readthedocs.io/en/latest) e com diversas adaptações para uso na infraestrutura Docker-Rancher do hacklab/.

## Ambiente de desenvolvimento

Para clonar este repositório, execute:

`git clone --recursive git@gitlab.com:ujs/ocupa-backend.git`

Se você já tiver a pasta de um submódulo (`ext-apps/ej-conversations`) no seu ambiente local, mas ela estiver vazia, execute o seguinte comando dentro dela:

`git submodule update --init --recursive`

Se você quiser receber as últimas atualizações aprovadas por outro desenvolvedor nos submódulos do projeto, execute:

`git submodule update --recursive`

Levante o ambiente de desenvolvimento com `docker-compose up` e acesse [localhost:8000](http://localhost:8000).

## Testes

Existem duas maneiras de se executar os testes automatizados localmente:

- Você já executou o comando `docker-compose up` e o servidor está funcionando.

```
docker-compose exec django pytest
```

- Você deseja apenas executar os testes sem necessariamente levantar o servidor. Antes é necessário construir a imagem do backend e disponibilizar o banco de dados para então executar o pytest via `docker run`

```
docker build -f compose/test/django/Dockerfile -t django_test .
docker run -d --env-file=./compose/test/test_env --name=postgres_test postgres:9.6
docker run --env-file=./compose/test/test_env --link=postgres_test:postgres \
  django_test /test.sh
```

## Variáveis de ambiente
### Banco de dados
- POSTGRES_HOST - opcional; padrão 'postgres'
- POSTGRES_DB - obrigatório
- POSTGRES_USER - obrigatório
- POSTGRES_PASSWORD - obrigatório

### Email
- MAILGUN_SENDER_DOMAIN - obrigatório em produção
- DJANGO_DEFAULT_FROM_EMAIL - obrigatório em produção
- DJANGO_MAILGUN_API_KEY - obrigatório em produção

### Django
- DJANGO_ALLOWED_HOSTS - obrigatório em produção
- DJANGO_ADMIN_URL - opcional
- DJANGO_SETTINGS_MODULE - opcional; use `config.settings.production` em produção
- DJANGO_ACCOUNT_ALLOW_REGISTRATION - opcional; padrão True
- DJANGO_SECRET_KEY - obrigatório em produção
- USE_CACHE - opcional; padrão True
- USE_DOCKER - opcional; desnecessário em produção; em ambientes locais, escreva 'yes' se estiver usando Docker

### Redis
- REDIS_URL - obrigatório em produção; exemplo: `redis://127.0.0.1:6379`

### Sentry
- DJANGO_SENTRY_DSN - opcional; só válido em produção


## Integrações de deploy
**Commits no branch `master`** fazem releases da versão em [**desenvolvimento**](https://dev.ocupa.org.br/).

**Tags** fazem releases em [**produção**](https://ocupa.org.br).
