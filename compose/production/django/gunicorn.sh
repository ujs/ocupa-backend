#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

python /app/manage.py migrate
python /app/manage.py compilemessages

/usr/local/bin/gunicorn config.wsgi -w 4 -b 0.0.0.0:5000 -t 600 --chdir=/app \
    --error-logfile=- \
    --access-logfile=- \
    --log-level info
