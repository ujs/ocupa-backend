from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework.documentation import include_docs_urls

from ocupa.users.views import FacebookLogin, TwitterLogin, OcupaLoginView, \
    OcupaPasswordResetView, OcupaPasswordResetConfirmView
from users.views import FacebookConnect, TwitterConnect, TwitterRequestToken, \
    TwitterAccessToken

urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, admin.site.urls),

    # Docs
    url(r'^api/docs/', include_docs_urls(title='ocupa API Docs', public=False)),

    # Address utilities
    url(r'^api/address/', include('ocupa.cities_api.urls', namespace='cities')),

    # User management
    url(r'^api/pages/', include('ocupa.pages.urls', namespace='pages')),
    url(r'^api/profile/', include('ocupa.users.urls', namespace='users')),
    url(r'^api/donation/', include('ocupa.donation.urls', namespace='donation')),

    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^rest-auth/twitter/$', TwitterLogin.as_view(), name='tw_login'),
    url(r'^rest-auth/facebook/connect/$', FacebookConnect.as_view(), name='fb_connect'),
    url(r'^rest-auth/twitter/request_token/$', TwitterRequestToken.as_view(), name='twitter_request_token'),
    url(r'^rest-auth/twitter/access_token/$', TwitterAccessToken.as_view(), name='twitter_access_token'),
    url(r'^rest-auth/twitter/connect/$', TwitterConnect.as_view(), name='twitter_connect'),
    url(r'^rest-auth/login/$', OcupaLoginView.as_view(), name='rest_login'),
    url(r'^rest-auth/password/reset/$', OcupaPasswordResetView.as_view(), name='rest_password_reset'),
    url(r'^rest-auth/password/reset/confirm/$', OcupaPasswordResetConfirmView.as_view(), name='rest_password_reset_confirm'),
    url(r'^rest-auth/', include('rest_auth.urls')),

    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^', include('django.contrib.auth.urls')),

    ## Dummy urls only to create reverse
    url(r'^recuperar-senha/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', TemplateView.as_view(), name='password_reset_confirm'),
    url(r'^verificar-email/(?P<key>[-:\w]+)/$', TemplateView.as_view(), name='account_confirm_email'),
    url(r'^verificar-email/$', TemplateView.as_view(), name='account_email_verification_sent'),
    url(r'^perfil/editar/twitter/connect/callback$', TemplateView.as_view(), name='twitter_connect_callback'),
    url(r'^campanha/(?P<slug>[0-9A-Za-z_\-]+)/cancelar-inscricao/(?P<code>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z_\-]+)$', TemplateView.as_view(), name='cancel_subscription'),

    url(r'^api/', include('ocupa.conversations.urls')),
    url(r'^api/', include('courier.urls', namespace='courier')),
    url(r'^discussion/', include('discussion.urls', namespace='discussion')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
